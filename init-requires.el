;;; init-requires.el --- my configuration (init-requires)

;; Copyright (C) 2015  HAMANO kiyoto

;; Author: HAMANO kiyoto <khiker.mail@gmail.com>
;; Keywords: .emacs init

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

(my-require 'my-config-check-updates)

(my-require 'my-frame)
(my-require 'my-package)
(my-require 'my-skk)
(my-require 'my-gnus)
(my-require 'my-mew)

(my-require 'my-browse-url)
(my-require 'my-buffer-c)
(my-require 'my-indent-c)
(my-require 'my-indent)
(my-require 'my-japan-util)
(my-require 'my-xdisp-c)
(my-require 'my-files)
(my-require 'my-eval-c)
(my-require 'my-startup)
(my-require 'my-paren)
(my-require 'my-frame-c)
(my-require 'my-scroll-bar)
(my-require 'my-simple)
(my-require 'my-dispnew-c)
(my-require 'my-minibuf-c)
(my-require 'my-minibuffer)
(my-require 'my-isearch)
(my-require 'my-url)
(my-require 'my-ffap)
(my-require 'my-window)
(my-require 'my-uniquify)
(my-require 'my-hl-line)
(my-require 'my-font-lock)
(my-require 'my-time)
(my-require 'my-dired)
(my-require 'my-ps-print)
(my-require 'my-elec-pair)
(my-require 'my-executable)

(my-require 'my-server)
(my-require 'my-imenu)
(my-require 'my-saveplace)
(my-require 'my-info)
(my-require 'my-woman)
(my-require 'my-view)
(my-require 'my-autoinsert)
(my-require 'my-autorevert)
(my-require 'my-tramp)
(my-require 'my-winner)

(my-require 'dabbrev-ja)
(my-require 'dabbrev-highlight)

(my-require 'my-popup)
(my-require 'my-popup-kill-ring)
(my-require 'my-popup-select-window)
(my-require 'my-migemo)
(my-require 'my-undo-tree)
;; use of winner-mode instead.
;; (my-require 'my-popwin)
(my-require 'my-auto-complete)
;; (my-require 'my-indent-guide)
(my-require 'my-lice)

(my-require 'my-xmodmap)
;; (my-require 'my-sdic)
;; (my-require 'my-elscreen)
(my-require 'my-tab-bar)


;; TODO:
;; Add following mode configurations
;;
;; - cperl
;; - python
;; - javascript
;; - coffeescript
;; - css
;; - scss
;; - scheme
(my-require 'my-flycheck)
(my-require 'my-sh-script)
(my-require 'my-c-c++)
(my-require 'my-emacs-lisp)
(my-require 'my-common-lisp)
(my-require 'my-ruby)
(my-require 'my-latex)
(my-require 'my-rust)
(my-require 'my-ede)
(my-require 'my-make-mode)
(my-require 'my-cmake)
(my-require 'my-text-mode)
(my-require 'my-rst)
(my-require 'my-org)

(my-require 'my-global-key)

(my-progn ";; Etc"
  (put 'dired-find-alternate-file 'disabled nil))

;;; init-requires.el ends here.
