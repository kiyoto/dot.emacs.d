(("init"    . ((nil . ((indent-tabs-mode . t)
		       (tab-width        . 8)
		       (buffer-read-only . t)))))
 ("init.el" . ((nil . ((indent-tabs-mode . t)
		       (tab-width        . 8)
		       (buffer-read-only . t)))))
 (emacs-lisp-mode
  . ((eval
      . (when (string-lessp (expand-file-name user-emacs-directory)
			    (expand-file-name default-directory))
	  (setq flycheck-emacs-lisp-load-path
		`(,(format "%sinit" user-emacs-directory)
		  ,(format "%ssite-lisp" user-emacs-directory))))))
  ))
