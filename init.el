;;; -*- Coding: utf-8-unix; Mode: Emacs-lisp -*-

;; Copyright (C) 2011-2017 HAMANO Kiyoto

;; Author: HAMANO Kiyoto <khiker.mail@gmail.com>
;; Keywords: .emacs init

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; This file tested on the Emacs 25.1.90 and 24.5. So, this file does
;; not consider the emacs 24.3 or less.
;;
;; And you also necessary to create ~/.emacs.d/envdep.el for user
;; depended environemt setting.
;;
;; you should install following packages.
;;
;; $ sudo apt-get install cmigemo fonts-horai-umefont
;; $ cargo install --git "https://github.com/rust-lang-nursery/rustfmt"
;; $ cargo install --git "https://github.com/phildawes/racer.git"

;;; Code:

;;; Basic configuration:

;; initalize packages
(package-initialize)

;; Set language environment to Japanese explicitly.
(set-language-environment "Japanese")

;; Load basic utilities.
(unless (load (format "%sinit/my.el" user-emacs-directory))
  (message ";; Load ERROR: init/my.el."))

;; Set character code to `utf-8-unix'.
(my-character-code-set 'utf-8-unix)

;; Set `load-path' to all subdirectories of ~/.emacs.d/site-lisp and
;; ~/.emacs.d/init.
(dolist (path `(,(format "%sinit" user-emacs-directory)
		,(format "%ssite-lisp" user-emacs-directory)))
  (my-load-path-set-recursively path))

;; Load environment dependency config file.
(when (file-exists-p my-environment-dependency-config-file)
  (my-load my-environment-dependency-config-file))

(setq load-prefer-newer t)

;; change the file that custom-set-file writes.
(my-load (setq custom-file (format "%scustom.el" user-emacs-directory)))

(setq user-full-name    (my-place-assoc my-user-full-name-alist)
      user-mail-address (my-place-assoc my-user-mail-address-alist))

;; the requiring under the init directory split the config files to make
;; the load-prefer-newer more effective.
(my-load (format "%sinit-requires.el" user-emacs-directory))

;;; init.el ends here
