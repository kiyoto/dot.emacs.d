;;; init --- my configuration (skk) -*- Mode: Emacs-Lisp -*-

;; Copyright (C) 2014-2015  HAMANO kiyoto

;; Author: HAMANO kiyoto <khiker.mail@gmail.com>
;; Keywords: .emacs init

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

(require 'skk)

(setq skk-use-jisx0201-input-method       t
      skk-inline-show-face                '(:foreground "white"
							:background "gray30"
							:underline t)
      skk-dcomp-activate                  t
      skk-dcomp-multiple-activate         t
      skk-egg-like-newline                t
      skk-japanese-message-and-error      t
      skk-auto-insert-paren               t
      skk-check-okurigana-on-touroku      t
      skk-show-inline                     'vertical
      skk-show-japanese-menu              t
      skk-number-style                    nil
      skk-show-annotation                 t
      skk-use-look                        t
      skk-use-auto-enclose-pair-of-region t)

(when (my-place-assoc my-skk-server-host)
  (setq skk-server-host (my-place-assoc my-skk-server-host)))

(when (my-place-assoc my-skk-server-port)
  (setq skk-server-portnum (my-place-assoc my-skk-server-port)))

(setq-default skk-kutouten-type 'en)

(defcustom my-skk-auto-save-jisyo-interval 600
  "Timer interval of auto saving the skk user jisyo."
  :type  'integer
  :group 'my)

(defvar my-skk-auto-save-jisyo nil)

(defun my-skk-auto-save-jisyo-start ()
  "Start the auto save timer of skk jisyo."
  (when my-skk-auto-save-jisyo
    (cancel-timer my-skk-auto-save-jisyo))
  (setq my-skk-auto-save-jisyo
	(run-with-idle-timer my-skk-auto-save-jisyo-interval
			     my-skk-auto-save-jisyo-interval
			     'skk-save-jisyo)))

(defun my-skk-auto-save-jisyo-stop ()
  "Stop the auto save timer of skk jisyo."
  (when my-skk-auto-save-jisyo
    (cancel-timer my-skk-auto-save-jisyo)
    (setq my-skk-auto-save-jisyo nil)))

(defun my-skk-find-file-hook-func ()
  "hook function for `find-file-hook' in skk."
  (skk-latin-mode-on))

(defun my-skk-minibuffer-setup-hook-func ()
  "hook function for `minibuffer-setup-hook' in skk."
  (skk-latin-mode-on)
  (minibuffer-message ""))

(defun my-skk-insert-advice (func &rest args)
  (let* ((keys (this-command-keys))
	 (key  (if (vectorp keys) (aref keys 0) keys))
	 (list (mapcar #'identity key))
	 (cmd  (nth 4 (skk-search-tree skk-rule-tree list))))
    (cond
     ((and (eq cmd 'skk-latin-mode) skk-j-mode buffer-read-only)
      (skk-latin-mode-on))
     (t
      (apply func args)))))
(advice-add 'skk-insert :around 'my-skk-insert-advice)

(my-skk-auto-save-jisyo-start)

(add-hook 'find-file-hook 'my-skk-find-file-hook-func)
(add-hook 'minibuffer-setup-hook 'my-skk-minibuffer-setup-hook-func)

;; turn skk-lantin-mode on.
(skk-latin-mode-on)

;;; init ends here.
