;;; skk-mazegaki.el --- Mazegaki henkan for SKK

;; Copyright (C) 2010, 2011  HAMANO Kiyoto

;; Author: HAMANO Kiyoto <khiker.mail+elisp@gmail.com>
;; Keywords: japanese, mule, input method, skk

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; SKK に交ぜ書き変換をする関数を提供する elisp です。
;; この機能を使うためには、交ぜ書き用の辞書が別途必要となります。しかし、
;; 交ぜ書き辞書が無くとも、漢字混じりの変換はできませんが、この機能自体
;; を使う事は可能です。

;;; Config:
;;
;; このファイルを `load-path' の通ったディレクトリに配置し、以下の設定
;; を .emacs に足して下さい。交ぜ書き用の辞書については、適宜、辞書サー
;; バの設定を編集する等して追加しておいて下さい。
;;
;;   (require 'skk-mazegaki)
;;   (setq skk-rom-kana-rule-list
;;             (append skk-rom-kana-rule-list
;;                    '(("fj" nil skk-mazegaki-henkan))))
;;   (setq skk-rule-tree
;;         (skk-compile-rule-list
;;          skk-rom-kana-base-rule-list skk-rom-kana-rule-list))

;;; Usage:
;;
;; 使用法は簡単です。「確定してしまったけど、やっぱり変換したい語句」の
;; 後ろで、上記設定例であれば、「fj」を押します。すると、再変換を行えま
;; す。この際、変換対象に漢字が混じった変換(=交ぜ書き変換)も行えます。
;; 変換対象の伸長や縮小は、デフォルトで以下のキーより行えます。
;;
;;   - 伸長: <
;;   - 縮小: >
;;   - 変換: SPC
;;   - 中止: C-g
;;
;; 例えば、「主きおくそうち」という語句であれば、以下の単語が変換候補と
;; して現れます。もちろん、これは私の環境での例であり、使用している辞書
;; により結果は異なります。
;;
;;   - 主きおくそうち
;;   - きおくそうち
;;   - そうち
;;   - うち
;;   - ち
;;
;; これらの候補を「<」、「>」キーで選択し、「SPC」キーで実際に変換を行
;; う事ができます。
;;
;; 変換候補として現れる単語は、実際に変換可能な語句のみです。また、現在
;; 変換候補として受け付ける文字列は大きく2種類です。
;;
;;   1. ひらがな、カタカナ、漢字から構成される文字列
;;   2. アルファベットと記号から構成される文字列
;;
;; 記号は、`skk-mazegaki-symbol-chars' に指定される記号が許容されます。
;;
;; また注意点として、「おこなった」のような促音混じりの変換を行う際は、
;; 「おこなっ」で一度変換を行い、その後、「た」を入力する事で変換を行う
;; 事ができます。
;;
;; なお、この交ぜ書き変換の関数は、SKK で日本語を入力するモードである
;; `skk-j-mode' 上でしか使えません。

;;; Compare:
;;
;; SKK に存在する既存の似た機能との比較
;;
;; * 通常の変換
;;   - 語句を確定した後に変換する事はできない。
;;   - デフォルトでは、漢字混じりの語句を変換できない
;;     # skk-tutcode だとできるが、変換済みの語句に対して変換をはじめら
;;     # れない
;;
;; * `skk-set-henkan-point-subr' 関数
;;   - 自分で変換の開始位置を決めなければならない。
;;
;; * `skk-backward-and-set-henkan-point' 関数 (M-q)
;;   - どこに変換の開始位置が飛ぶのか想像しづらい
;;   - 漢字混じりの語句を候補にする位置に飛べないように見える

;;; Known Bug:
;;
;; * fj で交ぜ書き変換モードに入り、候補を選択し、SPC で変換をはじめた
;;   後、C-g で取り止めると元あった文字が消えてしまう。
;;   # これは、`skk-backward-and-set-henkan-point' 関数でも同様の挙動と
;;   # なっており、対処は難しいように見える。
;;
;; * `skk-mazegaki-henkan' を呼ぶとポイント位置が変わってしまう。C-g を
;;   押しても元のポイント位置へと戻らない。

;;; Todo:
;;
;; * skk-mazegaki-henkan 関数の高速化
;;   現在は、変換対象となる文字列を分解し、その中から変換可能な語句のみ
;;   を抽出し、選択できるようにしている。これを、変換可能な最長の候補の
;;   探索のみに留め、変換候補の選択が発生した時点で次の候補の探索を行う
;;   ようにする。

;;; Specification:
;;
;; * Emacs 23 以上でしか動作しない
;;   「ひらがな」、「カタカナ」、「漢字」の切り出しをする際に、Unicode
;;   の範囲で指定しているため、Unicode 対応がなされている Emacs 23 以上
;;   でしか動作しない(未確認)。
;;
;; * 候補を選択し、実際に SPC を押して変換した後に、C-g を押して戻って
;;   も、再度候補の選択を行う事はできない。
;;   これは、現状ではどうしようもないと思う。
;;
;; * 変換の開始がちょっと遅い
;;   変換可能な語句のみを候補とするようにチェックを行っているので、その
;;   処理がどうしても重くなってしまう。

;;; Tested:
;;
;; * Emacs
;;   24.0.50
;; * Daredevil SKK
;;   14.1.50

;;; Thanks:
;;
;; 作成するにあたり、既存の SKK のコードと Emacs の T-Code/TUT-Code 実
;; 装 tc の交ぜ書き変換用コードを参考とさせて頂きました。ありがとうござ
;; います。
;;
;; * SKK Openlab
;;   - http://openlab.ring.gr.jp/skk/index-j.html
;; * tc2
;;   - http://openlab.ring.gr.jp/tcode/tc2/

;;; ChangeLog
;;
;; * 0.1.0 (2011/01/12)
;;  * skk-mazegaki-current-word 関数に英単語・記号の切り出し機能を追加。
;;  * 促音混じりの変換について注意を記載、それに応じて、Conern エントリ
;;    を削除
;;
;; * 0.0.4 (2011/01/12)
;;   * skk-mazegaki-search-candidates 関数にオプションの第3引数
;;     check-conversionable を追加。
;;   * skk-mazegaki-henkan 関数内で呼び出している
;;     skk-mazegaki-search-candidates の第3引数を t とした。
;;     # 処理の高速化が目的
;;
;; * 0.0.3 (2011/01/12)
;;   * skk-mazegaki-henkan 関数に前置引数があれば、
;;     skk-set-henkan-point-subr 関数を呼び出す機能を追加
;;
;; * 0.0.2 (2010/12/24)
;;   * skk-mazegaki-search-candidates に検索する辞書を指定する引数を追
;;     加
;;   * 懸案事項を書くコメントとして、Conern エントリをファイル頭に追加
;;
;; * 0.0.1 (2010/12/20)
;;   * Initial version

;;; Code:

(require 'skk)


(defconst skk-mazegaki-version "0.1.0"
  "skk-mazegaki のバージョン番号")


;; Variables:

(defvar skk-mazegaki-yomi-max 10
  "*現在のポイントからいくつ前の文字までを変換候補として解析するかを示
す変数

デフォルトでは、10 となっている。")

(defvar skk-mazegaki-ignore-regex-list
  '("[\n\r][ ]*")
  "*変換候補となる文字中で空文字列として扱う文字を示した正規表現のリス
トを収めた変数

例えば、「汽\nしゃ」という文字列を変換候補とした場合、改行「\n」は無視
され、「汽しゃ」という候補であると見做されて変換が行われる。")

(defvar skk-mazegaki-henkan-start-point-ignore-chars
  '(?。 ?、 ?． ?， ?・ ?  ?「 ?」 ?( ?) ?（ ?） ?\n)
  "*変換開始時のポイントの文字を変換対象とせず、その1つ前の文字を現在の
ポイントとして扱う際、無視する文字のリストを収めた変数

変換をはじめた際の現在のポイントがこのリスト中に示された文字のいずれか
であった場合、その文字を無視して、その1つ前の文字を現在のポイントとして
扱う")

(defvar skk-mazegaki-key-extention-list '(?<)
  "*変換候補を左に伸長するためのキー

デフォルトでは、「<」キーで候補の伸長を行う")

(defvar skk-mazegaki-key-shrink-list '(?>)
  "*変換候補を右に縮小するためのキー

デフォルトでは、「>」キーで候補の縮小を行う")

(defvar skk-mazegaki-key-henkan-list '(? )
  "*実際に変換を行うキー

デフォルトでは、SPC キーで候補の変換を行う")

(defvar skk-mazegaki-key-quit-list '(?)
  "*変換を中止するキー

デフォルトでは、`C-g' キーで変換の中止を行う")

(defvar skk-mazegaki-henkan-face 'underline
  "*変換候補に色をつけるためのフェイス

デフォルトでは、`underline' が付与される")

(defvar skk-mazegaki-symbol-chars
  '(?^ ?- ?= ?+ ?_ ?. ?, ?# ?@ ?! ?% ?$
       ?( ?) ?[ ?] ?/ ?\ ?' ?< ?> ?| ?~ ?` ?* ?& ??)
  "*変換候補とするシンボルのリスト
このリストに指定された文字は、`skk-mazegaki-current-word' 関数で変換対
象の文字として扱う。")


(defvar skk-mazegaki-yomi-points nil
  "skk-mazegaki のための内部変数")


;; Functions:

(defun skk-mazegaki-henkan (arg)
  "交ぜ書き変換を行う関数

以下の流れでこの関数は、実行される
1. 現在のポイントより前方に向かって、`skk-mazegaki-yomi-max' 文字数分だ
   け文字列を切り出す。
2. 切り出した文字列を解析し、変換可能な単語のみへと分解する。
3. 変換可能な単語を候補として、交ぜ書き変換を行う。"
  (interactive "*P")
  (when skk-j-mode
    (cond
     (arg
      (call-interactively 'skk-set-henkan-point-subr))
     (t
      (let* ((word (skk-mazegaki-current-word))
             (words (skk-mazegaki-breakup-word
                     word (cdr skk-mazegaki-yomi-points)))
             lis)
        ;; 変換可能な単語のみに候補を絞る
        (dolist (i words)
          (let ((p (car i))
                (w (cdr i)))
            (catch 'found
              (dolist (regex skk-mazegaki-ignore-regex-list)
                (when (string-match regex w)
                  (let* ((mstr   (match-string 0 w))
                         (bpoint (match-beginning 0))
                         (len (length mstr)))
                    (setq w (replace-match "" nil nil w))
                    (when (= bpoint 0)
                      (setq p (+ p len)))
                    (throw 'found nil)))))
            (when (and w
                       ;; 候補から、空文字列の削除
                       (not (string= w ""))
                       ;; 候補から、重複している単語の削除
                       (not (and (listp lis)
                                 (member w (mapcar #'(lambda (x) (cdr x))
                                                   lis))))
                       ;; 候補から、変換不可能な単語の削除
                       (skk-mazegaki-search-candidates
                        w skk-search-prog-list t))
              (setq lis (cons (cons p w) lis)))))
        ;; 実際に交ぜ書き変換を行う
        (skk-mazegaki-henkan-main (nreverse lis)))))))

(defun skk-mazegaki-henkan-main (candidates)
  "実際に交ぜ書き変換のメイン処理を行う関数

受け取った変換候補のリストより、候補の選択や挿入処理を一手に引き受ける。"
  (when (and candidates (> (length candidates) 0))
    (let* ((old-num -1)
           (now-num 0)
           (len (length candidates))
           (overlay nil)
           (delete-and-insert
            #'(lambda (sp ep w &optional face)
                (save-excursion
                  (unwind-protect
                      (delete-region sp ep)
                    (goto-char sp)
                    (insert w)
                    (when face
                      (setq overlay (make-overlay sp (+ sp (length w))))
                      (overlay-put overlay 'face skk-mazegaki-henkan-face))))))
           (yomi-sp (car skk-mazegaki-yomi-points))
           (action nil)
           ch candidate word-point word deleted-word inserted-word)
      (catch 'fin
        (while t
          (setq candidate (nth now-num candidates))
          (setq word-point (car candidate)
                word       (cdr candidate))
          (unwind-protect
              (progn
                (unless (= now-num old-num)
                  (setq deleted-word
                        (buffer-substring-no-properties word-point yomi-sp))
                  ;; 選択候補の挿入
                  (funcall delete-and-insert
                           word-point
                           (+ word-point (length deleted-word))
                           word
                           t))
                ;; 新規候補の選択
                (setq ch (aref (read-key-sequence-vector "") 0)
                      old-num now-num)
                (cond
                 ((member ch skk-mazegaki-key-extention-list)
                  (when (> now-num 0)
                    (setq now-num (1- now-num))))
                 ((member ch skk-mazegaki-key-shrink-list)
                  (when (> (1- len) now-num)
                    (setq now-num (1+ now-num))))
                 ((member ch skk-mazegaki-key-henkan-list)
                  (setq action 'henkan)
                  (throw 'fin nil))
                 ((member ch skk-mazegaki-key-quit-list)
                  (message "Quit")
                  (setq action 'quit)
                  (throw 'fin nil))
                 (t
                  (setq action 'insert)
                  (throw 'fin nil))))
            ;; 候補表示のために削除した単語を元に戻す
            (when (or (equal action 'quit)
                      (and (equal action nil)
                           (/= old-num now-num)))
              (funcall delete-and-insert
                       word-point
                       (+ word-point (length word))
                       deleted-word))
            (when (and overlay
                       (or (/= old-num now-num)
                           action))
              (delete-overlay overlay))
            ;; 実際に変換を行う
            (when (equal action 'henkan)
              (goto-char word-point)
              (skk-set-henkan-point-subr)
              (goto-char (+ 1 word-point (length word)))
              ;; 最初の候補への変換も同時に行う
              (let ((last-command-event ? ))
                (call-interactively 'skk-insert)))))))))

(defun skk-mazegaki-current-word ()
  "現在のポイントを起点として、起点ポイントより前方に向かって単語文字列
を切り出す関数。

起点となるポイントが、`skk-mazegaki-henkan-start-point-ignore-chars' に
含まれる語句であった場合、その文字を飛ばした場所から、単語文字列の取得
を開始する。飛ばした場所にある文字が、「アルファベット」もしくは、
`skk-mazegaki-symbol-chars' に指定された記号ならば、それら以外の文字が
来るまでの文字列を取得する(記号を含む英単語文字列の取得)。そうでない場
合、`skk-mazegaki-yomi-max' だけ単語文字列を取得する(日本語単語文字列の
取得)。

`skk-mazegaki-yomi-max' だけ取得した場合、文字列は、「ひらがな」、「カ
タカナ」、「漢字」で構成される。起点となるポイントから、
`skk-mazegaki-yomi-max' 文字数分前方までの間にそれ以外の文字があった場
合、単語の切り出しをそこで中止する。"
  (let ((start (cond
                ((member (char-before (point))
                         skk-mazegaki-henkan-start-point-ignore-chars)
                 (1- (point)))
                (t
                 (point))))
        (is-alphabet-or-symbol #'(lambda (ch)
                                   (or (and (>= ch ?a) (>= ?z ch))
                                       (and (>= ch ?A) (>= ?Z ch))
                                       (member ch skk-mazegaki-symbol-chars))))
        ch end)
    (setq skk-mazegaki-yomi-points nil)
    (cond
     ;; 英単語。もしくは、記号
     ((funcall is-alphabet-or-symbol (char-before start))
      (save-excursion
        (goto-char start)
        (while (and (not (bolp))
                    (funcall is-alphabet-or-symbol (char-before (point))))
          (goto-char (1- (point))))
        (setq end (point))))
     ;; 交ぜ書き変換対象文字列
     (t
      (save-excursion
        (goto-char start)
        (catch 'fin
          (dotimes (i skk-mazegaki-yomi-max)
            (setq ch (char-before (point)))
            (when (or (= (point) 1)
                      (not (or (or (= ch ? ) (= ch ?\n))
                               (and (>= ch #x3040) (>= #x309f ch)) ; ひらがな
                               (and (>= ch #x30a0) (>= #x30ff ch)) ; カタカナ
                               (and (>= ch #x3400) (>= #x2ffff ch))))) ; 漢字
              (throw 'fin nil))
            (goto-char (1- (point)))))
        (setq end (point)))))
    (when end
      (setq skk-mazegaki-yomi-points (cons start end))
      (buffer-substring start end))))

(defun skk-mazegaki-breakup-word (word &optional start-point)
  "単語 `word' を交ぜ書き変換用に分解してリストを返す関数。

例えば、「あいうえお」という文字列ならば、以下のようなリストが返される。

((0 . \"あいうえお\") (1 . \"いうえお\")
 (2 . \"うえお\") (3 . \"えお\") (4 . \"お\"))

0は、文字の開始インデックスを示す。オプションの第2引数 `start-point' を
指定すると、開始インデックスがその番号からとなる。"
  (let ((words (let ((counter (if start-point start-point 0)))
                 (mapcar '(lambda (x)
                            (prog1 (cons counter x)
                              (setq counter (1+ counter))))
                         (string-to-list word))))
        result)
    (dotimes (i (length words))
      (setq result (cons (cons (car (nth i words))
                               (mapconcat 'char-to-string
                                          (mapcar '(lambda (x)
                                                     (cdr x))
                                                  (nthcdr i words))
                                          nil))
                         result)))
    (nreverse result)))

(defun skk-mazegaki-search-candidates (word &optional prog-list
                                       check-conversionable)
  "単語 `word' の変換可能な候補全てを返す関数。

第2引数 `prog-list' の指定がなかった場合、
`skk-current-search-prog-list' として、'((skk-search-jisyo-file
skk-large-jisyo 10000)) が使われ、SKK-JISYO.L の内容でのみ検索される。

第3引数 `check-conversionable' に Non-nil が指定された場合、指定された
`word' に対して変換可能な候補が見付かった時点で新たな候補の探索を終了し、
見付けた候補を返す。

変数 `skk-auto-okuri-process' は、一時的に `nil' として変換を行う。"
  (let ((skk-henkan-key word)
        (skk-current-search-prog-list
         (cond
          (prog-list
           prog-list)
          (t
           '((skk-search-jisyo-file skk-large-jisyo 10000)))))
        (skk-auto-okuri-process nil)
        skk-henkan-list w)
    (while (and (not (and check-conversionable skk-henkan-list))
                (and skk-current-search-prog-list
                     (not w)))
      (setq skk-henkan-list (skk-nunion skk-henkan-list (skk-search))))
    (when skk-henkan-list
      (mapcar '(lambda (x)
                 (cond
                  ((string-match "^\\([^;]*\\);.*" x)
                   (match-string 1 x))
                  (t
                   x)))
              skk-henkan-list))))


(provide 'skk-mazegaki)

;;; skk-mazegaki.el ends here
