;;; align-let-autoloads.el --- automatically extracted autoloads
;;
;;; Code:
(add-to-list 'load-path (directory-file-name (or (file-name-directory #$) (car load-path))))

;;;### (autoloads nil "align-let" "align-let.el" (22609 26956 625555
;;;;;;  130000))
;;; Generated autoloads from align-let.el

(let ((loads (get 'align-let 'custom-loads))) (if (member '"align-let" loads) nil (put 'align-let 'custom-loads (cons '"align-let" loads))))

(put 'align-let-spaces 'safe-local-variable 'integerp)

(autoload 'align-let "align-let" "\
Align the value expressions in a Lisp `let' or `setq' form.
Point should be within or immediately in front of the let form.
For example `align-let' changes

    (let ((x 1)
          (foo   2)
          (zz (blah)))
      ...)
to
    (let ((x   1)
          (foo 2)
          (zz  (blah)))
      ...)

Or

    (setq x 1
          foo 2)
to
    (setq x   1
          foo 2)

When point is somewhere in the middle of the form, possibly
nested in an expression, the beginning of a let is found by
looking for a double-paren pattern like

    (sym ... ((...

This means `align-let' works with various kinds of `let', `let*',
etc in various Lisp dialects.  But possible `setq' functions are
hard-coded, currently `setq', `setq-default' and `psetq'.

See `align-let-spaces' to have more than one space after the
longest variable.

----------------------------------------------------------------
Scheme `and-let*' is recognised too,

    (and-let* (...

Its \"(var value)\" forms can be bare symbols so may not start
with a \"((\" like above.  You can have `align-let' recognise
other such forms by setting an `align-let' property on the symbol
in Emacs,

    (put 'my-and-let* 'align-let 1)

The property value is the argument number of the bindings part.
So for example if you made

    (my-xandlet foo bar (abc
                         (def 123)
                         (ghi 456))
      ...

then the bindings are the 3rd argument and you set

    (put 'my-xandlet 'align-let 3)

----------------------------------------------------------------
The align-let.el home page is
URL `http://user42.tuxfamily.org/align-let/index.html'

\(fn)" t nil)

(autoload 'align-let-keybinding "align-let" "\
Bind C-c C-a to `align-let' in the current mode keymap.
This is designed for use from the mode hook of any lisp-like
language, eg. `emacs-lisp-mode-hook' or `scheme-mode-hook'.

In the current implementation, if you `unload-feature' to remove
`align-let' the key bindings made here are not removed.  If you
use the `autoload' recommended in the align-let.el Install
\(manually or generated) then Emacs automatically re-loads on a
later `C-c C-a'.

\(fn)" nil nil)

(custom-add-option 'emacs-lisp-mode-hook 'align-let-keybinding)

(custom-add-option 'scheme-mode-hook 'align-let-keybinding)

(autoload 'align-let-region "align-let" "\
`align-let' all forms between START and END.
Interactively, align forms in the region between point and mark.

If a `let' form starts within the region but extends beyond END
then all its binding forms are aligned, even those past END.

One possibility for this command would be to combine it with
`indent-sexp' or `indent-pp-sexp' (\\[indent-pp-sexp]), or some sort
of \"indent defun\", for a one-key cleanup.  There's nothing
offered for that yet but it should be easy to make a combination
command according to personal preference.  It doesn't matter
whether you indent or align-let first, because align-let only
cares about variable name widths, not any indenting, and
indenting doesn't care about how much space there is after
variables!  Remember to use markers for the operative region, as
indent and align-let will both insert or delete characters.

\(fn START END)" t nil)

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; End:
;;; align-let-autoloads.el ends here
