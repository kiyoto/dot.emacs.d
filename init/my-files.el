;;; my-files.el --- my configuration (files)

;; Copyright (C) 2014-2015  HAMANO kiyoto

;; Author: HAMANO kiyoto <khiker.mail@gmail.com>
;; Keywords: .emacs init

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

(setq require-final-newline t
      confirm-kill-emacs 'y-or-n-p
      backup-inhibited t
      make-backup-files t
      save-abbrevs 'silently
      enable-remote-dir-locals t)

(let ((backup-directory (format "%sbak" user-emacs-directory)))
  (unless (file-exists-p backup-directory)
    (make-directory backup-directory))
  (dolist (i `(("\\.*$" . ,backup-directory)))
    (when (file-exists-p (cdr i))
      (add-to-list 'backup-directory-alist i))))

;; always starts the home direcotry.
(cd "~")

(provide 'my-files)

;;; my-files.el ends here
