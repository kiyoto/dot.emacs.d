;;; my-package.el --- my configuration (package)

;; Copyright (C) 2014-2015  HAMANO kiyoto

;; Author: HAMANO kiyoto <khiker.mail@gmail.com>
;; Keywords: .emacs init

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

(my-require 'package
  ;; package-initialize is done by init.el

  (dolist (arc '(("org"   . "http://orgmode.org/elpa/")
		 ("melpa" . "https://melpa.org/packages/")
		 ("elpy"  . "http://jorgenschaefer.github.io/packages/")))
    (add-to-list 'package-archives arc t))

  ;; workaround for user42.
  (setq package-check-signature nil)

  ;; always do update of package list metadata in all emacs startup.
  (package-refresh-contents)

  (when (boundp 'package-selected-packages)
    (dolist (package package-selected-packages)
      (unless (package-installed-p package)
	(package-install package)))))

(provide 'my-package)

;;; my-package.el ends here
