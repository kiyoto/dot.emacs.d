;;; my-isearch.el --- my configuration (isearch)

;; Copyright (C) 2014-2015  HAMANO kiyoto

;; Author: HAMANO kiyoto <khiker.mail@gmail.com>
;; Keywords: .emacs init

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

(eval-when-compile
  (require 'my))

(setq isearch-allow-scroll t)

(defun my-mode-line-isearch-mode-hook-func ()
  "Set mode-line string for isearch-mode."
  (setcar (cdr (assq 'isearch-mode minor-mode-alist)) ""))

(add-hook 'isearch-mode-hook 'my-mode-line-isearch-mode-hook-func)

(define-key isearch-mode-map (kbd "C-o")
  #'(lambda ()
      "search a current search word by `occur'."
      (interactive)
      (let ((case-fold-search isearch-case-fold-search))
	(occur (if isearch-regexp
		   isearch-string
		 (regexp-quote isearch-string))))))

(define-key isearch-mode-map (kbd "C-k")
  #'(lambda ()
      "Kill the current isearch match string and continue searching."
      (interactive)
      (kill-region isearch-other-end (point))))

(define-key isearch-mode-map (kbd "M-:")
  #'(lambda ()
      "comment out the line that includes a match word."
      (interactive)
      (save-excursion
	(comment-region (point-at-bol) (point-at-eol)))
      (if isearch-forward
	  (isearch-repeat-forward)
	(isearch-repeat-backward))))

(define-key isearch-mode-map (kbd "M-w")
  #'(lambda ()
      "Exit search normally. and save the `search-string' on kill-ring."
      (interactive)
      (isearch-done)
      (isearch-clean-overlays)
      (kill-new isearch-string)))

(my-require 'isearch-dabbrev
  (define-key isearch-mode-map (kbd "C-i") 'isearch-dabbrev-expand))

(provide 'my-isearch)

;;; my-isearch.el ends here
