;;; my-dired.el --- my configuration (dired)

;; Copyright (C) 2014-2015  HAMANO kiyoto

;; Author: HAMANO kiyoto <khiker.mail@gmail.com>
;; Keywords: .emacs init

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

(eval-when-compile
  (require 'my)
  (require 'dired-x)
  (declare-function dired-get-filename "dired")
  (declare-function dired-find-file "dired"))

(my-require 'dired-x)
(my-require 'sorter)

(setq dired-dwim-target t
      dired-recursive-copies  'always
      dired-recursive-deletes 'always
      dired-deletion-confirmer 'y-or-n-p
      dired-guess-shell-alist-user
      (list (list "\\.ps$" "gv * &")
	    (list "\\.pdf$" "acroread * &" "xpdf * &")
	    (list "\\.jpg$" "xv * &")
	    (list "\\.jpeg$" "xv * &")
	    (list "\\.png$" "eog * &")
	    (list "\\.gif$" "xv * &")
	    (list "\\.dvi$" "xdvi * &")
	    (list "\\.lzh$" "lha x *")
	    (list "\\.lzh$" "lha e *")))

(defun my-dired-advertised-find-file ()
  "Visite a file in Dired without making new buffer."
  (interactive)
  (let ((kill-target (current-buffer))
	(check-file (dired-get-filename)))
    (funcall 'dired-find-file)
    (if (file-directory-p check-file)
	(kill-buffer kill-target))))

(let ((map dired-mode-map))
  (define-key map (kbd "C-m") 'my-dired-advertised-find-file)
  (define-key map "W" 'wdired-change-to-wdired-mode)
  (define-key map (kbd ";") 'dired-find-alternate-file)
  (define-key map (kbd "C-/") 'dired-undo))

(provide 'my-dired)

;;; my-dired.el ends here
