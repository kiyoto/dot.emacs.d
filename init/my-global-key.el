;;; my-global-key.el --- my configuration (global-key)

;; Copyright (C) 2014-2015  HAMANO kiyoto

;; Author: HAMANO kiyoto <khiker.mail@gmail.com>
;; Keywords: .emacs init

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

(defmacro my-set-key (&rest plist)
  (let ((def (plist-get plist :default))
	(reg (plist-get plist :region)))
    `(lambda ()
       (interactive)
       (cond
	,(when reg
	   `(mark-active
	     (funcall ,reg (region-beginning) (region-end))))
	(t
	 ,(cond
	   (def
	    `(call-interactively ,def))
	   (t
	    `ignore)))))))

(defun my-forward-word (arg)
  "forward-word that considers japanese words."
  (interactive "p")
  (let ((char-category
	 '(lambda (ch)
	    (when ch
	      (let* ((c (char-category-set ch))
		     ct)
		(cond
		 ((aref c ?a)
		  (cond
		   ((or (and (>= ?z ch) (>= ch ?a))
			(and (>= ?Z ch) (>= ch ?A))
			(and (>= ?9 ch) (>= ch ?0))
			(= ch ?-) (= ch ?_))
		    'alphnum)
		   (t
		    'ex-alphnum)))
		 ((aref c ?j) ; Japanese
		  (cond
		   ((aref c ?K) 'katakana)
		   ((aref c ?A) '2alphnum)
		   ((aref c ?H) 'hiragana)
		   ((aref c ?C) 'kanji)
		   (t 'ja)))
		 ((aref c ?k) 'hankaku-kana)
		 ((aref c ?r) 'j-roman)
		 (t 'etc))))))
	(direction 'char-after)
	char type)
    (when (null arg) (setq arg 1))
    (when (> 0 arg)
      (setq arg (- arg))
      (setq direction 'char-before))
    (while (> arg 0)
      (setq char (funcall direction))
      (setq type (funcall char-category char))
      (while (and (prog1 (not (eq (point) (point-max)))
		    (cond ((eq direction 'char-after)
			   (goto-char (1+ (point))))
			  (t
			   (goto-char (1- (point))))))
		  (eq type (funcall char-category (funcall direction)))))
      (setq arg (1- arg)))
    type))

(defun my-backward-word (arg)
  "backward-word that considers japanese words."
  (interactive "p")
  (my-forward-word (- (or arg 1))))

(defun my-other-window-backward (arg)
  (interactive "p")
  (other-window (- arg)))

(defun my-shrink-window-enlarge (arg)
  (interactive "p")
  (shrink-window (- arg)))

(global-set-key (kbd "C-h")      'backward-delete-char)
(global-set-key (kbd "C-c h")    'help-command)
(global-set-key (kbd "C-j")      'newline)
(global-set-key (kbd "C-m")      'indent-new-comment-line)
(global-set-key (kbd "M-j")      'newline-and-indent)
(global-set-key (kbd "C-x v e")  'ediff-revision)
(global-set-key (kbd "C-c x")    'shell-command)
(global-set-key (kbd "C-x 4 A")  'add-change-log-entry)
(global-set-key (kbd "C-:")      'other-window)
(global-set-key (kbd "C-;")      'my-other-window-backward)
(global-set-key (kbd "C-M-;")    'other-frame)
(global-set-key (kbd "M-f")      'my-forward-word)
(global-set-key (kbd "M-b")      'my-backward-word)
(global-set-key (kbd "C-<up>")   'shrink-window)
(global-set-key (kbd "C-<down>") 'my-shrink-window-enlarge)

(global-set-key (kbd "C-w")
		(my-set-key :default #'backward-kill-word
			    :region  #'kill-region))

(provide 'my-global-key)

;;; my-global-key.el ends here
