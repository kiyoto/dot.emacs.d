;;; my-text-mode.el --- my configuration (text-mode)

;; Copyright (C) 2015  HAMANO kiyoto

;; Author: HAMANO kiyoto <khiker.mail@gmail.com>
;; Keywords: .emacs init

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

(eval-when-compile
  (require 'my)
  (require 'org))

(defun my-text-mode-hook-func ()
  ;; use font-locking in the text-mode.
  (font-lock-mode t)
  (font-lock-ensure))

(add-hook 'text-mode-hook #'my-text-mode-hook-func)

(provide 'my-text-mode)

;;; my-text-mode.el ends here
