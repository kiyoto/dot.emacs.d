;;; my-popup.el --- my configuration (popup)

;; Copyright (C) 2014-2015  HAMANO kiyoto

;; Author: HAMANO kiyoto <khiker.mail@gmail.com>
;; Keywords: .emacs init

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

(eval-when-compile
  (require 'my)
  (require 'popup)
  (require 'flymake)
  (declare-function popup-tip "popup")
  (declare-function flymake-find-err-info "flymake"))

(my-require '(popup flymake)
  ;; XXX
  (defun my-popup-flymake-display-error ()
    "display flymake message by `popup-tip'."
    (interactive)
    (let* ((linum (line-number-at-pos))
	   (linfo (nth 0 (flymake-find-err-info flymake-err-info linum)))
	   (len   (length linfo)))
      (dotimes (i len)
	(when linfo
	  (let* ((num  (1- (- len i)))
		 (line (flymake-ler-line (nth num linfo)))
		 (text (flymake-ler-text (nth num linfo))))
	    (popup-tip (format "[%s] %s" line text))))))))

(provide 'my-popup)

;;; my-popup.el ends here
