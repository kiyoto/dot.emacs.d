;;; my-ede.el --- my configuration (ede)

;; Copyright (C) 2014-2015  HAMANO kiyoto

;; Author: HAMANO kiyoto <khiker.mail@gmail.com>
;; Keywords: .emacs init

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

(eval-when-compile
  (require 'my)
  (require 'ede)
  (require 'semantic)
  (require 'stickyfunc-enhance)
  (declare-function global-semantic-idle-scheduler-mode "semantic/idle")
  (declare-function global-semantic-highlight-func-mode "semantic/util-modes"))

(my-require '(ede semantic stickyfunc-enhance)
  (global-ede-mode t)
  (semantic-mode t)

  (global-semantic-idle-scheduler-mode t)
  ;; (global-semantic-idle-completions-mode t)
  ;; (global-semantic-decoration-mode t)
  (global-semantic-highlight-func-mode t)
  ;; (global-semantic-show-unmatched-syntax-mode t)

  (setq semantic-default-submodes
	'(global-semantic-idle-scheduler-mode
	  global-semanticdb-minor-mode
	  global-semantic-idle-completions-mode
	  global-semantic-highlight-func-mode
	  global-semantic-decoration-mode
	  ;; ;; global-semantic-stickyfunc-mode makes Emacs so slow when
	  ;; ;; buffer size is too large (e.g. more than 7000 lines).
	  ;; global-semantic-stickyfunc-mode
	  global-semantic-mru-bookmark-mode))
  (setq-default semantic-stickyfunc-sticky-classes
		'(function type variable include package)))

(provide 'my-ede)

;;; my-ede.el ends here
