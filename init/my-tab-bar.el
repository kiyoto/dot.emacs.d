;;; my-tab-bar.el --- my configuration (tab-bar)                      -*- lexical-binding: t; -*-

;; Copyright (C) 2023  HAMANO Kiyoto

;; Author: HAMANO Kiyoto <khiker.mail@gmail.com>
;; Keywords: .emacs init

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

(eval-when-compile
  (require 'my))

(my-require 'tab-bar
  (setq tab-bar-new-tab-choice #'(lambda () (get-buffer-create "*scratch*")))
  (setq tab-bar-tab-hints t)
  (setq tab-bar-new-tab-to 'rightmost)

  (defvar my-tab-map
    (let ((keymap (make-sparse-keymap)))
      (dotimes (i 10)
        (define-key keymap (kbd (format "%d" i)) #'(lambda () (interactive) (tab-bar-select-tab i)))
        (define-key keymap (kbd (format "C-%d" i)) #'(lambda () (interactive) (tab-bar-select-tab i))))
      (define-key keymap (kbd "=")   #'tab-rename)
      (define-key keymap (kbd "d")   #'tab-close)
      (define-key keymap (kbd "C-d") #'tab-close)
      (define-key keymap (kbd "n")   #'tab-next)
      (define-key keymap (kbd "C-n") #'tab-next)
      (define-key keymap (kbd "p")   #'tab-previous)
      (define-key keymap (kbd "C-p") #'tab-previous)
      (define-key keymap (kbd "c")   #'tab-new)
      (define-key keymap (kbd "C-c") #'tab-new)
      (define-key keymap (kbd "C-f") #'find-file-other-tab)
      (define-key keymap (kbd "w")   #'tab-switch)
      (define-key keymap (kbd "C-w") #'tab-switcher)

      ;; C-z C-z is suspend frame. This is useful in emacs nw.
      (define-key keymap (kbd "C-z") 'suspend-frame)
      keymap))

  (define-key tab-switcher-mode-map (kbd "q") #'tab-close)

  (global-set-key (kbd "C-z") my-tab-map)
  (global-set-key (kbd "C-,") #'tab-previous)
  (global-set-key (kbd "C-.") #'tab-next)

  (tab-bar-mode 1))


(provide 'my-tab-bar)

;;; my-tab-bar.el ends here
