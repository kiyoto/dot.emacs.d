;;; my-font.el --- my configuration (font)

;; Copyright (C) 2014-2015  HAMANO kiyoto

;; Author: HAMANO kiyoto <khiker.mail@gmail.com>
;; Keywords: .emacs init font

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

(defcustom my-font-fontset-alist
  '(("umegothic14_base"
     "梅ゴシック-14"
     "梅ゴシック-14"
     "IPAゴシック-14"
     "IPAゴシック-14"
     "花園明朝OT xProN-14")

    ("umegothic09_base"
     "梅ゴシック-09"
     "梅ゴシック-09"
     "IPAゴシック-09"
     "IPAゴシック-09"
     "花園明朝OT xProN-09")

    ;; $ sudo apt install fontforge fonts-migmix
    ;; $ git clone https://github.com/yascentur/Ricty.git
    ;; $ cd Ricty
    ;; $ wget https://github.com/google/fonts/raw/master/ofl/inconsolata/Inconsolata-Bold.ttf
    ;; $ wget https://github.com/google/fonts/raw/master/ofl/inconsolata/Inconsolata-Regular.ttf 
    ;; $ ./ricty_generator.sh auto
    ("ricty07_base"
     "Ricty-7"
     "Ricty-7"
     "Ricty-7"
     "Ricty-7"
     "Ricty-7")

    ("ricty09_base"
     "Ricty-9"
     "Ricty-9"
     "Ricty-9"
     "Ricty-9"
     "Ricty-9")

    ("ricty12_base"
     "Ricty-12"
     "Ricty-12"
     "Ricty-12"
     "Ricty-12"
     "Ricty-12")

    ;; sudo apt-get install fonts-noto fonts-noto-cjk fonts-noto-mono
    ("notosans12_base"
     "Noto Sans Mono CJK JP-12"
     "Noto Sans Mono CJK JP-12"
     "Noto Sans Mono CJK JP-12"
     "Noto Sans Mono CJK JP-12"
     "Noto Sans Mono CJK JP-12")

    ("anonymouse12_base"
     "-unknown-Anonymous-normal-normal-normal-*-12-*-*-*-m-0-iso10646-1"
     "-unknown-IPAゴシック-normal-normal-normal-*-16-*-*-*-d-0-iso10646-1"
     "-unknown-IPAゴシック-normal-normal-normal-*-16-*-*-*-d-0-iso10646-1"
     "-unknown-IPA明朝-normal-normal-normal-*-16-*-*-*-d-0-iso10646-1"
     "-unknown-花園明朝OT xProN-normal-normal-normal-*-16-*-*-*-m-0-iso10646-1")

    ("anonymouse14_base"
     "-unknown-Anonymous-normal-normal-normal-*-14-*-*-*-m-0-iso10646-1"
     "-unknown-IPAゴシック-normal-normal-normal-*-18-*-*-*-d-0-iso10646-1"
     "-unknown-IPAゴシック-normal-normal-normal-*-18-*-*-*-d-0-iso10646-1"
     "-unknown-IPA明朝-normal-normal-normal-*-18-*-*-*-d-0-iso10646-1"
     "-unknown-花園明朝OT xProN-normal-normal-normal-*-18-*-*-*-m-0-iso10646-1")

    ("anonymouse18_base"
     "-unknown-Anonymous-normal-normal-normal-*-18-*-*-*-m-0-iso10646-1"
     "-unknown-IPAゴシック-normal-normal-normal-*-24-*-*-*-d-0-iso10646-1"
     "-unknown-IPAゴシック-normal-normal-normal-*-24-*-*-*-d-0-iso10646-1"
     "-unknown-IPA明朝-normal-normal-normal-*-24-*-*-*-d-0-iso10646-1"
     "-unknown-花園明朝OT xProN-normal-normal-normal-*-24-*-*-*-m-0-iso10646-1")

    ("profont14_base"
     "-unknown-ProFontWindows-normal-normal-normal-*-14-*-*-*-m-0-iso10646-1"
     "-unknown-IPAゴシック-normal-normal-normal-*-18-*-*-*-d-0-iso10646-1"
     "-unknown-IPAゴシック-normal-normal-normal-*-18-*-*-*-d-0-iso10646-1"
     "-unknown-IPA明朝-normal-normal-normal-*-18-*-*-*-d-0-iso10646-1"
     "-unknown-花園明朝OT xProN-normal-normal-normal-*-18-*-*-*-m-0-iso10646-1"))
  "The alist of fontset definition."
  :type  '(repeat
	   (list :tag "fontset definition"
		 (string :tag "fontset name")
		 (string :tag "fontname (ascii)")
		 (string :tag "fontname (jix0208)")
		 (string :tag "fontname (hiragana) ")
		 (string :tag "fontname (katakana)")
		 (string :tag "fontname (kanji)")))
  :group 'my)

(defun my-font-fontset-create (name ascii jisx0208 hiragana katakana kanji)
  "create the fontset.
The created fontset name is \(concat \"fontset-\" name\)."
  (catch 'font-unfound
    (progn
      (dolist (font-name (list ascii jisx0208 hiragana kanji kanji))
	(unless (font-info font-name)
	  (message "W: Font %s does not found." font-name)
	  (throw 'font-unfound nil)))
      (let ((fontset-name (create-fontset-from-ascii-font ascii nil name)))
	(set-fontset-font fontset-name 'japanese-jisx0208  jisx0208)
	(set-fontset-font fontset-name '(#x301c . #x301d)  jisx0208)
	(set-fontset-font fontset-name '(#x3040 . #x309f)  hiragana)
	(set-fontset-font fontset-name '(#x30a0 . #x30ff)  katakana)
	(set-fontset-font fontset-name '(#x3400 . #x2ffff) kanji)))
    t))

(defun my-font-set (font-name)
  "Set font `font-name' to default font."
  (dolist (i '(default-frame-alist initial-frame-alist))
    (cond
     ((and (listp i) (assoc 'font i))
      (setf (cdr (assoc 'font i)) font-name))
     (t
      (add-to-list i `(font . ,font-name)))))
  (set-frame-font font-name nil t)
  (set-face-attribute 'default t :font font-name))

(defun my-font-init ()
  (interactive)
  (when window-system
    (let ((fontname (my-place-assoc my-font-alist))
          (fontsets (my-place-assoc my-fontset-alist))
          (created-font-list '()))
      (dolist (alist (list my-font-fontset-alist fontsets))
        (dolist (list alist)
          (when (apply 'my-font-fontset-create list)
            (add-to-list 'created-font-list (nth 0 list)))))
    (message "%s %s" created-font-list fontname)
    (when (member fontname created-font-list)
      (my-font-set (format "fontset-%s" fontname))))))

(provide 'my-font)

;;; my-font.el ends here
