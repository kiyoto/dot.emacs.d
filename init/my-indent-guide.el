;;; my-indent-guide.el --- my configuration (indent-guide)

;; Copyright (C) 2014-2015  HAMANO kiyoto

;; Author: HAMANO kiyoto <khiker.mail@gmail.com>
;; Keywords: .emacs init

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

(eval-when-compile
  (require 'my)
  (require 'indent-guide))

(my-require 'indent-guide
  (add-to-list 'indent-guide-inhibit-modes 'package-menu-mode)
  (add-to-list 'indent-guide-inhibit-modes 'mew-summary-mode)
  (add-to-list 'indent-guide-inhibit-modes 'mew-virtual-mode)
  (indent-guide-global-mode))

(provide 'my-indent-guide)

;;; my-indent-guide.el ends here
