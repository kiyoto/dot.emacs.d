;;; my-gnus.el --- my configuration (gnus)

;; Copyright (C) 2014-2015  HAMANO kiyoto

;; Author: HAMANO kiyoto <khiker.mail@gmail.com>
;; Keywords: .emacs init

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

(eval-when-compile
  (require 'my)
  (require 'gnus)
  (require 'gnus-start)
  (require 'gnus-art)
  (require 'gnus-cache)
  (require 'gnus-msg)
  (require 'gnus-agent)
  (require 'auth-source)
  (require 'bbdb)
  (require 'starttls)
  (require 'nnimap)
  (require 'nnir)
  (require 'smtpmail)
  (declare-function gnus-extra-header "gnus-sum")
  (declare-function gnus-uncompress-range "gnus-range"))

(my-require '(gnus gnus-start gnus-art auth-source
		   starttls nnimap nnir)
  (defvar my-gnus-mail-addres-regex (my-place-assoc
				     my-user-mail-address-regex-alist)
    "Regular expression of mail address that indicates for me.")

  (defvar my-gnus-summary-maximum-articles 500
    "The recent X number of articles that displayed in summary-buffer
by use `gnus-topic-select-group' (RET) in gnus-group-buffer. The default
value is 500. The recent 500 articles are always displayed at least.")

  (defun gnus-user-format-function-j (headers)
    "Return a \">\" if variable `my-gnus-mail-addres-regex' matches in To,
CC or Bcc. If not matched, return a \" \"."
    (cond
     ((or (string-match my-gnus-mail-addres-regex
			(gnus-extra-header 'To headers))
	  (string-match my-gnus-mail-addres-regex
			(gnus-extra-header 'Cc headers))
	  (string-match my-gnus-mail-addres-regex
			(gnus-extra-header 'BCc headers)))
      ">")
     (t
      " ")))

  (defun my-gnus-read-article (group articles)
    "Display always `my-gnus-summary-maximum-articles' articles
at least in summary buffer."
    (let ((active (gnus-active group)))
      (delete-dups
       (append articles
	       (gnus-uncompress-range
		(cond
		 (my-gnus-summary-maximum-articles
		  ;; show `my-gnus-summary-maximum-articles' messages.
		  (cons (max (car active)
			     (- (cdr active)
				my-gnus-summary-maximum-articles
				-1))
			(cdr active)))
		 (t
		  ;; show always all messages.
		  active)))))))


  ;; write following line to the ~/.emacs.d/.authinfo.gpg.
  ;;
  ;; machine imap.gmail.com loging foobar password foobar
  ;; machine smtp.gmail.com loging foobar password foobar port 587
  (setq auth-sources `(,(format "%s.authinfo.gpg" user-emacs-directory)
		       ,(format "%s.netrc" user-emacs-directory)))

  ;; for reading
  (setq gnus-select-method
	`(nnimap "mail"
		 (nnimap-address       ,(my-place-assoc
					 my-user-imap-address-alist))
		 (nnimap-server-port   ,(my-place-assoc
					 my-user-imap-port-alist))
		 (nnimap-stream        ssl)))

  ;; for sending
  (setq message-send-mail-function 'smtpmail-send-it
	message-confirm-send          t
	send-mail-function            'smtpmail-send-it
	smtpmail-default-smtp-server  (my-place-assoc
				       my-user-smtp-address-alist)
	smtpmail-smtp-server          (my-place-assoc
				       my-user-smtp-address-alist)
	smtpmail-smtp-service         (my-place-assoc
				       my-user-smtp-port-alist)
	message-default-mail-headers  "Cc: \nBcc: \n")

  (setq gnus-summary-line-format "%uj%U%R%I%(%[%-23,23f%]%) %s\n")

  ;; gnu-topic-mode by default
  (add-hook 'gnus-group-mode-hook 'gnus-topic-mode)

  ;; MUA is gnus.
  (setq read-mail-command 'gnus
	mail-user-agent   'gnus-user-agent)

  (setq gnus-startup-file (format "%s.newsrc" user-emacs-directory))

  (setq gnus-check-new-newsgroups      nil
	gnus-use-cache                 t
	gnus-cache-directory           "~/Mail/cache/"
	gnus-cache-enter-articles      '(ticked dormant read unread)
	gnus-cache-remove-articles     nil
	gnus-cacheable-groups          "^nnimap"
	gnus-posting-styles            `((".*"
				          (name
				           ,(my-place-assoc
					     my-user-full-name-alist))))
	gnus-keep-backlog              nil
	gnus-use-correct-string-widths t
	gnus-agent-go-online           t
	gnus-group-goto-unread         nil
	gnus-visible-headers           (concat gnus-visible-headers
					       "\\|^User-Agent\\|^X-Mailer")
	gnus-extra-headers             '(To
					 Newsgroups
					 X-Newsreader
					 Content-Type
					 CC
					 User-Agent
					 Gnus-Warning)
	nnmail-extra-headers           gnus-extra-headers
	gnus-fetch-old-headers         t)

  ;; (setq gnus-alter-articles-to-read-function #'my-gnus-read-article)
  (setq gnus-alter-articles-to-read-function nil)

  ;; Gnus + EasyPG
  (my-require '(epg-config gnus-msg mml2015)
    (setq gnus-message-replysign              t
	  gnus-message-replyencrypt           t
	  gnus-message-replysignencrypted     t
	  mm-verify-option                    'always
	  mm-decrypt-option                   'always
	  mml2015-use                         'epg
	  mml-secure-openpgp-encrypt-to-self  t
	  mml-secure-openpgp-always-trust     nil
	  mml-secure-cache-passphrase         t
	  mml-secure-passphrase-cache-expiry  '36000
	  mml-secure-openpgp-sign-with-sender t
	  gnus-buttonized-mime-types          '("multipart/alternative"
						"multipart/encrypted"
						"multipart/signed")))

  ;; mail contacts list manager.
  (my-require 'bbdb
    (setq bbdb-user-mail-address-re (my-place-assoc
				     my-user-mail-address-regex-alist))

    (bbdb-initialize 'gnus 'message)))

(provide 'my-gnus)

;;; my-gnus.el ends here
