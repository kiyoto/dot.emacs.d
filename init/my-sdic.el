;;; my-sdic.el --- my configuration (sdic) -*- coding: utf-8; lexical-binding:t -*-

;; Copyright (C) 2014-2015  HAMANO kiyoto

;; Author: HAMANO kiyoto <khiker.mail@gmail.com>
;; Keywords: .emacs init

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

(eval-when-compile
  (require 'my)
  (require 'sdic)
  (require 'sdicf)
  (require 'sdic-inline)
  (require 'sdic-inline-pos-tip)
  (declare-function sdicf-array-init "sdicf")
  (declare-function sdic-inline-display-minibuffer "sdic-inline")
  (declare-function sdic-inline-pos-tip-show "sdic-inline"))

(defun my-sdic-move-item-advice ()
  (recenter 0))

(defun my-sdic-search-advice (query)
  (save-excursion
    (highlight-phrase query "hi-yellow")))

(defun my-sdic-init (eiwa waei)
  (with-eval-after-load "sdic"
    (setq sdic-eiwa-dictionary-list `((sdicf-client ,eiwa))
	  sdic-waei-dictionary-list `((sdicf-client ,waei)))))

(defun my-sdic-init-sary (sary-path eiwa waei)
  (let ((path sary-path))
    (with-eval-after-load "sdic"
      (setq sdicf-array-command path)

      ;; set location of sdic dictionary files (eiwa only).
      ;; set to use array command
      (setq sdic-eiwa-dictionary-list `((sdicf-client ,eiwa (strategy array)))
	    sdic-waei-dictionary-list `((sdicf-client ,waei (strategy array))))

      ;; replace forcely a function for array command that defined in
      ;; sdicf.el to use sary directly.
      (fset 'sdicf-array-init 'sdicf-common-init)
      (fset 'sdicf-array-quit 'sdicf-common-quit)
      (fset 'sdicf-array-search
	    #'(lambda (sdic pattern &optional case regexp)
		(sdicf-array-init sdic)
		(cond
		 (regexp
		  (signal 'sdicf-invalid-method '(regexp)))
		 (t
		  (save-excursion
		    (with-current-buffer (sdicf-get-buffer sdic)
		      (erase-buffer)
		      (apply 'sdicf-call-process
			     sdicf-array-command
			     (sdicf-get-coding-system sdic)
			     nil
			     t
			     nil
			     (list (if case "-i")
				   pattern
				   (sdicf-get-filename sdic)))
		      (goto-char (point-min))
		      (let (entries)
			(while (not (eobp)) (sdicf-search-internal))
			(nreverse entries)))))))))))

(let ((eiwa (my-place-assoc my-sdic-eiwa-dictionary-alist))
      (waei (my-place-assoc my-sdic-waei-dictionary-alist))
      (sary (executable-find "sary")))
  (when (and (file-exists-p eiwa) (file-exists-p waei))
    (cond
     (sary
      (my-sdic-init-sary sary eiwa waei))
     (t
      (my-sdic-init eiwa waei)))

    ;; When moving inside dictionary buffer, move point to first line of
    ;; the buffer always.
    (advice-add 'sdic-forward-item  :after 'my-sdic-move-item-advice)
    (advice-add 'sdic-backward-item :after 'my-sdic-move-item-advice)

    (global-set-key "\C-xw" 'sdic-describe-word)
    (global-set-key "\C-xW" 'sdic-describe-word-at-point)

    (advice-add 'sdic-search-eiwa-dictionary :after 'my-sdic-search-advice)
    (advice-add 'sdic-search-waei-dictionary :after 'my-sdic-search-advice)

    (my-require '(sdic-inline sdic-inline-pos-tip)
      ;; do not run sdic-inline by default.

      (setq sdic-inline-eiwa-dictionary eiwa
	    sdic-inline-waei-dictionary waei)

      (dolist (i '(font-lock-doc-face
		   help-mode
		   Info-mode
		   texinfo-mode
		   org-mode
		   nxml-mode
		   latex-mode
		   text-mode
		   review-mode
		   fundamental-mode
		   gnus-article-mode
		   rst-mode
		   po-mode))
	(add-to-list 'sdic-inline-enable-modes i))

      (defun sdic-inline-pos-tip-show-when-region-selected (entry)
	(cond
	 ((and transient-mark-mode mark-active)
	  (funcall 'sdic-inline-pos-tip-show entry))
	 (t
	  (funcall 'sdic-inline-display-minibuffer entry))))

      (setq sdic-inline-search-func 'sdic-inline-search-word-with-stem
	    sdic-inline-display-func
	    'sdic-inline-pos-tip-show-when-region-selected)
      (define-key sdic-inline-map "\C-c\C-p" 'sdic-inline-pos-tip-show))))

(provide 'my-sdic)

;;; my-sdic.el ends here
