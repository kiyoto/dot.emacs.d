;;; my-emacs-lisp.el --- my configuration (emacs-lisp)

;; Copyright (C) 2014-2015  HAMANO kiyoto

;; Author: HAMANO kiyoto <khiker.mail@gmail.com>
;; Keywords: .emacs init

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

(eval-when-compile
  (require 'my))

(defun my-lispxmp-emacs-lisp ()
  (interactive)
  (let ((comment-start ";"))
    (comment-kill nil)
    (comment-indent)
    (save-excursion
      (let ((current-prefix-arg t))
	(call-interactively 'eval-last-sexp)))
    (insert " => ")))

(defun my-emacs-lisp-mode-hook-func ()
  "Hook function for `emacs-lisp-mode-hook'."
  (my-require 'jump
    (define-key emacs-lisp-mode-map (kbd "C-c j") 'jump-symbol-at-point)
    (define-key emacs-lisp-mode-map (kbd "C-c b") 'jump-back))

  (my-require 'highlight-defined
    (highlight-defined-mode))

  (my-require 'align-let
    (align-let-keybinding))

  (my-require 'elisp-slime-nav
    (elisp-slime-nav-mode))

  (my-require '(flycheck flycheck-tip)
    (define-key emacs-lisp-mode-map (kbd "C-c d") 'flycheck-tip-cycle))

  (eldoc-mode)

  (define-key emacs-lisp-mode-map (kbd "C-c C-e") 'my-lispxmp-emacs-lisp))

(add-hook 'emacs-lisp-mode-hook 'my-emacs-lisp-mode-hook-func)

(provide 'my-emacs-lisp)

;;; my-emacs-lisp.el ends here
