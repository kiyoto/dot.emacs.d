;;; my.el --- my configuration (utilities)

;; Copyright (C) 2014-2015  HAMANO kiyoto

;; Author: HAMANO kiyoto <khiker.mail@gmail.com>
;; Keywords: .emacs init

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

(defconst my-environment-dependency-config-file
  (format "%senvdep.el" user-emacs-directory)
  "Path of the file that has environment dependency configurations.")

(defgroup my nil
  "my configuration set."
  :tag   "my"
  :group 'my)

(defcustom my-place 'home
  "Current place. home, laptop, office and so on. The default
value is home."
  :tag   "place"
  :type  'symbol
  :group 'my)

(defcustom my-user-full-name-alist '((home . "HAMANO Kiyoto"))
  "The alist of user-full-name."
  :type  '(repeat (cons :tag "rule"
			(symbol :tag "place")
			(string :tag "user full name")))
  :group 'my)

(defcustom my-user-mail-address-alist '((home . "khiker.mail@gmail.com"))
  "The alist of user-mail-address."
  :type  '(repeat (cons :tag "rule"
			(symbol :tag "place")
			(string :tag "mail address")))
  :group 'my)

(defcustom my-user-mail-address-regex-alist
  '((home . "khiker\\.mail\\(\\+[^@]+\\)?@gmail\\.com"))
  "The alist of user-mail-address regex."
  :type  '(repeat (cons :tag "rule"
			(symbol :tag "place")
			(string :tag "mail address regex")))
  :group 'my)

(defcustom my-user-imap-address-alist '((home . "imap.gmail.com"))
  "The alist of iamp mail server address."
  :type  '(repeat (cons :tag "rule"
			(symbol :tag "place")
			(string :tag "imap mail server address")))
  :group 'my)

(defcustom my-user-imap-port-alist '((home . 993))
  "The alist of imap mail server port."
  :type  '(repeat (cons :tag "rule"
			(symbol  :tag "place")
			(integer :tag "imap mail server port")))
  :group 'my)

(defcustom my-user-smtp-address-alist '((home . "smtp.gmail.com"))
  "The alist of smtp mail server address."
  :type  '(repeat (cons :tag "rule"
			(symbol :tag "place")
			(string :tag "smtp mail server address")))
  :group 'my)

(defcustom my-user-smtp-port-alist '((home . 587))
  "The alist of smtp mail server port."
  :type  '(repeat (cons :tag "rule"
			(symbol :tag "place")
			(string :tag "smtp address regex")))
  :group 'my)

(defcustom my-frame-width '((home . 100))
  "The alist of frame-width."
  :type  '(repeat (cons :tag "rule"
			(symbol :tag "place")
			(string :tag "width")))
  :group 'my)

(defcustom my-frame-height '((home . 50))
  "The alist of frame-height."
  :type  '(repeat (cons :tag "rule"
			(symbol :tag "place")
			(string :tag "height")))
  :group 'my)

(defcustom my-frame-position-x '((home . 0))
  "The alist of frame x position."
  :type  '(repeat (cons :tag "rule"
			(symbol :tag "place")
			(string :tag "x position")))
  :group 'my)

(defcustom my-frame-position-y '((home . 0))
  "The alist of frame y position."
  :type  '(repeat (cons :tag "rule"
			(symbol :tag "place")
			(string :tag "y position")))
  :group 'my)

(defcustom my-font-alist '((home . "anonymouse18_base"))
  "The alist of fontset to be used."
  :type  '(repeat (cons :tag "rule"
			(symbol :tag "place")
			(string :tag "fontset name")))
  :group 'my)

(defcustom my-fontset-alist '((home . nil))
  "The alist of additional fontset definitions."
  :type  '(repeat (cons :tag "rule"
			(symbol :tag "place")
			(repeat
			 (list :tag "fontset definition"
			       (string :tag "fontset name")
			       (string :tag "fontname (ascii)")
			       (string :tag "fontname (jix0208)")
			       (string :tag "fontname (hiragana) ")
			       (string :tag "fontname (katakana)")
			       (string :tag "fontname (kanji)")))))
  :group 'my)

(defcustom my-migemo-bin-path '((home . "cmigemo"))
  "The alist of migemo binary to be used."
  :type  '(repeat (cons :tag "rule"
			(symbol :tag "place")
			(string :tag "migemo command path")))
  :group 'my)

(defcustom my-migemo-dictionary-path
  '((home . "/usr/share/cmigemo/utf-8/migemo-dict"))
  "The alist of migemo dictionary to be used."
  :type  '(repeat (cons :tag "rule"
			(symbol :tag "place")
			(string :tag "migemo dictionary path")))
  :group 'my)

(defcustom my-sdic-eiwa-dictionary-alist
  '((home . "~/local/share/dict/eijirou.sdic"))
  "The alist of eiwa dictionary."
  :type  '(repeat (cons :tag "rule"
			(symbol :tag "place")
			(string :tag "path of eiwa dictionary")))
  :group 'my)

(defcustom my-sdic-waei-dictionary-alist
  '((home . "~/local/share/dict/waeijirou.sdic"))
  "The alist of waei dictionary."
  :type  '(repeat (cons :tag "rule"
			(symbol :tag "place")
			(string :tag "path of waei dictionary")))
  :group 'my)

(defcustom my-skk-server-host '((home . nil))
  "The alist of skk server host to be used."
  :type  '(repeat (cons :tag "rule"
			(symbol :tag "place")
			(string :tag "skk server host")))
  :group 'my)

(defcustom my-skk-server-port '((home . nil))
  "The alist of skk server port to be used."
  :type  '(repeat (cons :tag "rule"
			(symbol :tag "place")
			(integer :tag "skk server port")))
  :group 'my)

(defcustom my-hyperspec-path-alist
  '((home . "/usr/share/doc/hyperspec"))
  "The alist of waei dictionary."
  :type  '(repeat (cons :tag "rule"
			(symbol :tag "place")
			(string :tag "path of waei dictionary")))
  :group 'my)

(defvar my-loading-status '(:success 0 :failed 0))

(defsubst my-place-assoc (alist)
  "Return configuration of current place in the `alist'."
  (let ((val (assoc my-place alist)))
    (cond
     (val
      (cdr val))
     (t
      (message (concat ";; Caution %S is nil. "
		       "Used the default value (value in home).")
	       alist)
      (cdr (assoc 'home alist))))))

(defmacro my-require (feature &rest body)
  "Do `require' safely. If `require' of feature `feature' is
succeeded, Output a message \"Require SUCCESS ...\".  If failed,
Output a message \"Require FAILED\"."
  (declare (indent 1))
  `(when (cond
	  ((listp ,feature)
	   (not (member nil (mapcar #'(lambda (f)
					(if (require f nil t)
					    t
					  (message "Require FAILED: %s" f)
					  (plist-put my-loading-status :failed
						     (1+ (plist-get my-loading-status
								    :failed)))
					  (sit-for 1)
					  nil))
				    (delete-dups ,feature)))))
	  (t
	   (if (require ,feature nil t)
	       (progn
		 ,@body)
	     (message "Require FAILED: %s" ,feature)
	     (plist-put my-loading-status :failed
			(1+ (plist-get my-loading-status :failed)))
	     (sit-for 1)
	     nil)))
     (message ";; Require SUCCESS: %s" ,feature)
     (plist-put my-loading-status :success
		(1+ (plist-get my-loading-status :success)))))

(defmacro my-require! (feature &rest body)
  "Do not `require' the feature `feature' and body `body'. "
  (declare (indent 1))
  nil)

(defmacro my-load (name &rest body)
  "Do `load' safely. If `load' of feature `load' is succeeded,
Output a message \"Load SUCCESS ...\".  If failed, Output a
message \"Load FAILED\"."
  (declare (indent 1))
  `(if (load ,name t)
       (progn
	 (message ";; Load SUCCESS: %s" ,name)
	 (plist-put my-loading-status :success
		    (1+ (plist-get my-loading-status :success)))
	 ,@body)
     (message ";; Load ERROR: %s" ,name)
     (plist-put my-loading-status :failed
		(1+ (plist-get my-loading-status :failed)))
     (sit-for 1)))

(defmacro my-load! (feature &rest body)
  "Do not `load' the feature `feature' and body `body'. "
  (declare (indent 1))
  nil)

(defmacro my-progn (msg &rest body)
  "Output message `msg' and execute body `body'."
  (declare (indent 1))
  `(progn
     (message ,msg)
     ,@body))

(defmacro my-progn! (msg &rest body)
  "Do not execute body `body'."
  (declare (indent 1))
  nil)

(defun my-character-code-set (code)
  "Set character code to `code'."
  (interactive)
  (let ((code (if (called-interactively-p 'interactive)
		  (completing-read "Select chara codes: "
				   (mapcar #'(lambda (x)
					       (list x x))
					   coding-system-list)
				   nil t)
		code)))
    (set-default-coding-systems	   code)
    (set-buffer-file-coding-system code)
    (set-terminal-coding-system	   code)
    (set-keyboard-coding-system	   code)
    (prefer-coding-system	   code)
    (setq-default buffer-file-coding-system code)))

(defun my-directory-files-recursively (dir)
  "Do directory-files recursively. This function ignores `.' and
`..' directories."
  (let* (files
	 (my-directory-files1
	  #'(lambda (base-dir &optional subdir)
	      (let ((dir1 base-dir))
		(when subdir
		  (setq dir1 (concat dir1 "/" subdir))
		  (add-to-list 'files (format "%s/%s/" base-dir subdir)))
		(dolist (i (directory-files dir1))
		  (cond
		   ((file-directory-p (concat dir1 "/" i))
		    (unless (member (file-relative-name i)
				    '("." ".." "./" "../"))
		      ;; For free variable reference
		      (with-no-warnings
			(funcall my-directory-files1
				 base-dir (concat subdir "/" i)))))
		   (t
		    (cond
		     (subdir
		      (add-to-list 'files
				   (format "%s/%s/%s" base-dir subdir i)))
		     (t
		      (add-to-list 'files (format "%s/%s" base-dir i)))))))))))
    (funcall my-directory-files1 dir)
    files))

(defmacro my-file-add-to-list (alist elem)
  "Set `elem' to `alist' safely."
  `(let ((elem (expand-file-name ,elem)))
    (when (and (file-exists-p elem) (not (member elem ,alist)))
       (add-to-list ',alist elem))))

(defun my-load-path-set-recursively (base-dir)
  "Set load-path to all subdirectories of `dir'." ;
  (interactive)
  (when (and base-dir (file-directory-p base-dir))
    (my-file-add-to-list load-path base-dir)
    (dolist (i (my-directory-files-recursively base-dir))
      (when (and (not (member (file-relative-name i) '("." "..")))
		 (file-directory-p i))
	(my-file-add-to-list load-path i)))))

(provide 'my)

;;; my.el ends here
