;;; my-hl-line.el --- my configuration (hl-line)

;; Copyright (C) 2014-2015  HAMANO kiyoto

;; Author: HAMANO kiyoto <khiker.mail@gmail.com>
;; Keywords: .emacs init

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

(eval-when-compile
  (require 'my)
  (require 'hl-line)
  (declare-function hl-line-make-overlay "hl-line")
  (declare-function global-hl-line-unhighlight "hl-line")
  (declare-function global-hl-line-highlight "hl-line"))

(global-hl-line-mode t)

(defface my-hl-line-face
  '((((class color) (background dark))
     (:background "DarkSlateBlue" t))
    (((class color) (background light))
     (:background "LightGreen" t))
    (t (:bold t)))
  "hl-line's my face"
  :group 'hl-line)

(setq hl-line-face 'my-hl-line-face)

;; Distinguish the read-only state by hl-line mode color
(defface my-hl-line-readonly-face
  '((t (:background "DarkRed")))
  "hl-line's my face for read-only buffer"
  :group 'hl-line)

(defun my-hl-line-readonly-hook-function ()
  (let ((f (if buffer-read-only
               'my-hl-line-readonly-face
             'my-hl-line-face)))
    (when global-hl-line-overlay
      (overlay-put global-hl-line-overlay 'face f))
    (setq hl-line-face f)))

(add-hook 'view-mode-hook 'my-hl-line-readonly-hook-function)
(add-hook 'find-file-hook 'my-hl-line-readonly-hook-function)

;; switch line color at switching read-write buffer and read-only
;; buffer.
(advice-add #'global-hl-line-highlight
	    :before #'my-hl-line-readonly-hook-function)

(provide 'my-hl-line)

;;; my-hl-line.el ends here
