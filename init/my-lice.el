;;; my-lice.el --- my configuration (lice) -*- lexical-binding: t; -*-

;; Copyright (C) 2016  HAMANO Kiyoto

;; Author: HAMANO Kiyoto <khiker.mail@gmail.com>
;; Keywords: .emacs init

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

(eval-when-compile
  (require 'my)
  (require 'lice))

(my-require 'lice
  ;; NOTE: lice.el becomes error by bytecompiling because loop macro.
  ;; r350 fix that to change lice.el directly.
  (add-to-list 'lice:license-directories
	       (format "%s/etc/lice/" user-emacs-directory)))

(provide 'my-lice)

;;; my-lice.el ends here
