;;; my-simple.el --- my configuration (simple)

;; Copyright (C) 2014-2015  HAMANO kiyoto

;; Author: HAMANO kiyoto <khiker.mail@gmail.com>
;; Keywords: .emacs init

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

(setq kill-ring-max 1000
      kill-do-not-save-duplicates t
      kill-whole-line t
      set-mark-command-repeat-pop t
      eval-expression-print-length 10000
      eval-expression-print-level 10000)

(column-number-mode t)
(line-number-mode t)

;; when edit a read-only file by C-x C-q, ask to open that file as root
;; by using a tramp sudo method.
(defun my-read-only-open-as-root ()
  (let ((name (buffer-file-name))
	(pos  (point)))
    (when (and (not buffer-read-only)
	       name
	       (file-exists-p name)
	       (not (file-writable-p name))
	       (y-or-n-p (format "File %s is read-only. open it as root?" name)))
      (when (find-alternate-file (format "/sudo::%s" name))
	(goto-char pos)))))

(add-hook 'read-only-mode-hook #'my-read-only-open-as-root)

(provide 'my-simple)

;;; my-simple.el ends here
