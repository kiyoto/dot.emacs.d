;;; my-frame.el --- my configuration (frame)

;; Copyright (C) 2014-2015  HAMANO kiyoto

;; Author: HAMANO kiyoto <khiker.mail@gmail.com>
;; Keywords: .emacs init

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

(eval-when-compile
  (require 'my)
  (declare-function my-font-init "my-font"))

(set-face-foreground 'mode-line "white")
(set-face-background 'mode-line "black")

;; frame configurations.
(dolist (i '(default-frame-alist initial-frame-alist))
  (dolist (j `((background-color . "black")
	       (foreground-color . "green")
	       (width  . ,(my-place-assoc my-frame-width))
	       (height . ,(my-place-assoc my-frame-height))
	       (left   . ,(my-place-assoc my-frame-position-x))
	       (top    . ,(my-place-assoc my-frame-position-y))
	       (cursor-color . "gray")))
    (add-to-list i j)))

  ;; font configurations.
(my-require 'my-font
  (declare-function my-after-make-frame-functions-func "my-frame")
  (defun my-after-make-frame-functions-func (&rest frame)
    ;; in tty environment (-nw or --daemon), font is not initialized. in
    ;; that case, when make a frame by emacsclient -c, do the initialize
    ;; of the font.
    (my-font-init))

  (add-hook 'after-make-frame-functions
	    #'my-after-make-frame-functions-func)

  (my-font-init))

(provide 'my-frame)

;;; my-frame.el ends here
