;;; my-common-lisp.el --- my configuration (common-lisp)

;; Copyright (C) 2014-2015  HAMANO kiyoto

;; Author: HAMANO kiyoto <khiker.mail@gmail.com>
;; Keywords: .emacs init

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

(eval-when-compile
  (require 'my)
  (require 'skk)
  (require 'slime)
  (require 'inferior-slime)
  (require 'slime-autodoc)
  (require 'ac-slime)
  (declare-function skk-insert "skk")
  (declare-function slime-space "slime")
  (declare-function slime-autodoc-mode "slime-autodoc"))

(defun my-skk-slime-space-insert (n)
  "for space problem of between the skk and the slime."
  (interactive "p")
  (cond
   (skk-henkan-mode
    (skk-insert))
   (t
    (slime-space n))))

(setq inferior-lisp-program "sbcl --noinform")

(my-require '(slime inferior-slime)
  (slime-setup)

  (setq slime-net-coding-system 'utf-8-unix
	slime-lisp-implementations
	'((sbcl  ("sbcl")  :coding-system utf-8-unix)
	  (clisp ("clisp") :coding-system utf-8-unix))))

(my-require 'hyperspec
  (let ((path (my-place-assoc my-hyperspec-path-alist)))
    (setq common-lisp-hyperspec-root         (format "file://%s/" path)
	  common-lisp-hyperspec-symbol-table
	  (format "file://%s/Data/Map_Sym.txt" path))))

(defun my-slime-mode-hook-func ()
  "Hook function for `slime-mode-hook'."
  (let ((keymap slime-mode-map))
    (define-key keymap (kbd "s-;") 'slime-selector)
    (define-key keymap (kbd " ")   'my-skk-slime-space-insert))

  (my-require 'ac-slime
    (set-up-slime-ac)))

(defun my-slime-repl-mode-hook-func ()
  (my-require 'ac-slime
    (set-up-slime-ac)))

(defun my-lisp-mode-hook-func ()
  "Hook function for `lisp-mode-hook'."
  (my-require 'slime-autodoc
    (slime-autodoc-mode))

  (my-require 'slime
    (slime-mode t))

  (my-require 'ac-slime
    (eval-after-load "auto-complete"
      '(add-to-list 'ac-modes 'slime-repl-mode)))

  (define-key lisp-mode-map (kbd "C-c s")   'slime-selector)
  (define-key lisp-mode-map (kbd "C-c H")   'hyperspec-lookup)
  (define-key lisp-mode-map (kbd "C-c C-e") 'lispxmp-slime))

(defun my-inferior-lisp-mode-hook-func ()
  "Hook function for `inferior-lisp-mode-hook'."
  (define-key inferior-slime-mode-map (kbd "s-;") 'slime-selector))

(add-hook 'lisp-mode-hook          'my-lisp-mode-hook-func)
(add-hook 'inferior-lisp-mode-hook 'my-inferior-lisp-mode-hook-func)
(add-hook 'slime-mode-hook         'my-slime-mode-hook-func)
(add-hook 'slime-repl-mode-hook    'my-slime-repl-mode-hook-func)

(provide 'my-common-lisp)

;;; my-common-lisp.el ends here
