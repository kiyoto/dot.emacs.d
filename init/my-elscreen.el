;;; my-elscreen.el --- my configuration (elscreen)

;; Copyright (C) 2014-2015  HAMANO kiyoto

;; Author: HAMANO kiyoto <khiker.mail@gmail.com>
;; Keywords: .emacs init

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

(eval-when-compile
  (require 'my)
  (require 'navbar)
  (require 'navbarx-elscreen)
  (require 'elscreen)
  (declare-function navbarx-elscreen "navbarx-elscreen")
  (declare-function navbar-buffer-name "navbar")
  (declare-function navbar--serialize "navbar")
  (declare-function navbar--expand-glues "navbar")
  (declare-function navbar-buffer "navbar")
  (declare-function navbar-advices-setup "navbar")
  (declare-function navbar-make-window "navbar")
  (declare-function navbar-initialize "navbar")
  (declare-function navbar-update "navbar")
  (declare-function navbar-deinitialize "navbar")
  (declare-function navbar-advices-teardown "my-elscreen")
  (declare-function navbar-mode "navbar")
  (declare-function elscreen-get-number-of-screens "elscreen")
  (declare-function elscreen-message "elscreen")
  (declare-function elscreen-get-screen-list "elscreen")
  (declare-function elscreen-set-window-configuration "elscreen")
  (declare-function elscreen-get-current-screen "elscreen")
  (declare-function elscreen-current-window-configuration "elscreen")
  (declare-function elscreen-default-window-configuration "elscreen")
  (declare-function elscreen-append-screen-to-history "elscreen")
  (declare-function elscreen-notify-screen-modification "elscreen")
  (declare-function elscreen-goto "elscreen")
  (declare-function elscreen-get-screen-to-name-alist "elscreen")
  (declare-function elscreen-status-label "elscreen")
  (declare-function elscreen-truncate-screen-name "elscreen")
  (declare-function elscreen-tab-escape-% "elscreen")
  (declare-function elscreen-get-previous-screen "elscreen")
  (declare-function my-navbar-update-by-hook "my-elscreen")
  (declare-function my-elscreen-tab-list "my-elscreen")
  (declare-function my-elscreen-tab-number "my-elscreen")
  (declare-function my-elscreen-tab-width "my-elscreen")
  (declare-function my-elscreen-tab-show "my-elscreen")
  (declare-function my-elscreen-list-mode "my-elscreen")
  (declare-function my-elscreen-list-quit "my-elscreen"))

(my-require '(navbar navbarx-elscreen)
  ;; show elscreen tab to the navbar buffer instead of header-line of
  ;; buffer. if show elscreen tab to the header-line, it collapsed in
  ;; the case of splitting buffer vertically.
  (add-to-list 'navbar-item-list #'navbarx-elscreen t)

  ;; set face-remapping-alist to the buffer of navbar.
  (defun my-navbar-buffer-create (&optional frame)
    (let* ((name (navbar-buffer-name frame))
	   (buffer (get-buffer name)))
      (unless buffer
	(setq buffer (get-buffer-create name))
	(with-current-buffer buffer
	  (setq mode-line-format nil)
	  (setq cursor-type nil)
	  (setq truncate-lines t)
	  (setq window-size-fixed 'height)
	  (setq face-remapping-alist '((navbar :height 0.8)))
	  (use-local-map navbar-base-map)
	  (buffer-face-set 'navbar)))
      buffer))
  (advice-add 'navbar-buffer-create :override 'my-navbar-buffer-create)

  ;; in the advice to the next-window function, avoid the infinite loop.
  ;; it is necessary to avoid the " *popwin-dummy*" and navbar-window.
  (defun my-navbar-advice-next-window (orig-func &optional window minibuf all-frames)
    (let ((first-window (or window (selected-window)))
	  retval)
      (setq retval (funcall orig-func window minibuf all-frames))
      (let (last-window)
	(while (and (not (eq retval first-window))
		    (window-parameter retval 'navbar-window)
		    ;; if next-window function returns same return value
		    ;; in twice continuation, break this loop.
		    (and (not (null last-window))
			 (not (eq last-window retval))))
	  (setq last-window retval)
	  (setq window retval)
	  (setq retval (funcall orig-func window minibuf all-frames))))
      ;; FIXME: this workaround is very harmfull!
      (when (eq (buffer-name (window-buffer retval)) "  *popwin-dummy*")
	(setq retval first-window))
      retval))

  (defun my-navbar-advice-window-list (orig-func &optional frame minibuf window)
    (let (retval)
      (setq retval (funcall orig-func frame minibuf window))
      (setq retval
	    (cl-loop for window in retval
		     unless (window-parameter window 'navbar-window)
		     collect window))
      retval))

  (defun my-navbar-advices-setup ()
    (advice-add 'next-window :around 'my-navbar-advice-next-window)
    (advice-add 'window-list :around 'my-navbar-advice-window-list))
  (advice-add 'navbar-advices-setup :override 'my-navbar-advices-setup)

  (defun navbar-advices-teardown ()
    ;; Ignore already removed advices
    (ignore-errors
      (advice-remove 'next-window 'my-navbar-advice-next-window)
      (advice-remove 'window-list 'my-navbar-advice-window-list)))

  ;; i prefer alphabet X to \u00d7.
  (setq navbarx-elscreen-kill-screen
	(concat (propertize "[X]"
			    'keymap navbarx-elscreen-kill-screen-map
			    'help-echo 'navbarx-elscreen-kill-screen-help)
		;; Reset the keymap for the right item
		(propertize " " 'display '(space :width 0))))


  ;; workaround for emacs 25.1 --daemon + emacsclient -nw
  ;;
  ;; when start emacsclient -nw after the emacs --daemon in the emacs
  ;; 25.1, emacsclient crash. that cause probably seems
  ;; with-current-buffer execution of navbar-display function. to avoid
  ;; that, in the execution of navbar-display by emacsclient -nw, change
  ;; not to execute with-current-buffer. as a that result, since
  ;; emacsclient -nw does not show navbar, for countermeasure, execute
  ;; navbar-update with with-current-buffer by C-l
  ;; (recenter-top-bottom).
  (defun my-navbar-display (item-list buffer &optional check-graphics)
    "Display serialized ITEM-LIST in BUFFER."
    (let* ((values (navbar--serialize item-list))
	   (strings (cl-loop for value in values
			     when (stringp value)
			     collect value)))
      (unless (equal values strings)
	(setq strings (navbar--expand-glues
		       values strings (get-buffer-window buffer))))
      (when (or (null check-graphics) (display-graphic-p))
	(with-current-buffer buffer
	  (let (deactivate-mark)
	    (erase-buffer)
	    (insert (apply #'concat strings)))))))
  (advice-add 'navbar-display :override 'my-navbar-display)

  (defun my-navbar-update (&optional frame check-graphics)
    "Update navbar of FRAME."
    (unless frame
      (setq frame (selected-frame)))
    ;; workaround for emacs 26+. navbar window does not be displayed
    ;; when elscreen-create was run. since navbar-update will run, run
    ;; navbar-make-window evenry time at that timing.
    (when (>= emacs-major-version 26)
      (navbar-make-window frame))
    (with-selected-frame frame
      (funcall navbar-display-function
	       (mapcar #'cdr navbar-item-alist)
	       (navbar-buffer frame)
	       check-graphics)))
  (advice-add 'navbar-update :override 'my-navbar-update)

  (defun my-navbar-kill-buffer-and-window (&optional frame)
    (unless frame
      (setq frame (selected-frame)))
    (let ((window (navbar-window frame))
	  (buffer (navbar-buffer frame)))
      (when window
	;; emacs 26+, delete-side-window function does not exists.
	(let ((window-combination-resize
	       (window-parameter (window-parent window) 'window-side))
	      (ignore-window-parameters t))
	  (delete-window window)))
      (when buffer
	(kill-buffer buffer))))
  (advice-add 'navbar-kill-buffer-and-window :override 'my-navbar-kill-buffer-and-window)

  (defun my-navbar-update-by-hook (&optional frame)
    ;; suppress "avbar-update called with 2 arguments, but accepts only 0-1".
    (with-no-warnings (navbar-update frame t)))

  (defun my-navbar-setup ()
    (navbar-advices-setup)
    (add-hook 'after-make-frame-functions #'my-navbar-update-by-hook)
    (add-hook 'after-make-frame-functions #'navbar-make-window)
    (add-hook 'window-size-change-functions #'my-navbar-update-by-hook)
    (mapc #'navbar-make-window (frame-list))
    (navbar-initialize)
    (mapc #'navbar-update (frame-list))
    (font-lock-add-keywords 'emacs-lisp-mode navbar-font-lock-keywords))
  (advice-add 'navbar-setup :override 'my-navbar-setup)

  (defun my-navbar-teardown ()
    (navbar-deinitialize)
    (navbar-advices-teardown)
    (remove-hook 'after-make-frame-functions #'my-navbar-update-by-hook)
    (remove-hook 'after-make-frame-functions #'navbar-make-window)
    (remove-hook 'window-size-change-functions #'my-navbar-update-by-hook)
    (mapc 'navbar-kill-buffer-and-window (frame-list))
    (font-lock-remove-keywords 'emacs-lisp-mode navbar-font-lock-keywords))
  (advice-add 'navbar-teardown :override 'my-navbar-teardown)

  (advice-add 'recenter-top-bottom :after 'navbar-update)


  ;; this navbar add the following local changes.
  ;; - navbarx-elscreen-kill-screen function uses X character instead of
  ;;   \u00d7 for closing button.
  ;; - navbar-buffer-create function do following.
  ;;   (setq face-remapping-alist '((navbar :height 0.8))))
  (navbar-mode))

(my-require 'elscreen
  ;; *. ELSCREEN EXTENDED SCREEN NUMBER
  ;; ------------------------------------------------------------
  ;;
  ;; the default max screen number of elscreen is small for me.  So, I
  ;; extend max screen number of elscreen to 36 (0 ... 9, a ... z).
  (defvar my-elscreen-tab-max 36)

  (defun my-elscreen-create-internal (&optional noerror)
    "Create a new screen.
If NOERROR is not nil, no message is displayed in mini buffer
when error is occurred."
    (let ((max (if (and (numberp my-elscreen-tab-max)
			(>= my-elscreen-tab-max 10))
		   my-elscreen-tab-max
		 10)))
      (cond
       ((>= (elscreen-get-number-of-screens) max)
	(unless noerror
	  (elscreen-message "No more screens."))
	nil)
       (t
	(let ((screen-list (sort (elscreen-get-screen-list) '<))
	      (screen 0))
	  (elscreen-set-window-configuration
	   (elscreen-get-current-screen)
	   (elscreen-current-window-configuration))
	  (while (eq (nth screen screen-list) screen)
	    (setq screen (+ screen 1)))
	  (elscreen-set-window-configuration
	   screen (elscreen-default-window-configuration))
	  (elscreen-append-screen-to-history screen)
	  (elscreen-notify-screen-modification 'force)
	  (run-hooks 'elscreen-create-hook)
	  screen)))))

  (advice-add 'elscreen-create-internal :override 'my-elscreen-create-internal)

  (defun my-elscreen-jump ()
    "Switch to specified screen."
    (interactive)
    (let ((next-screen (+ (- (string-to-char (string last-command-event)) ?a)
			  10)))
      (if (and (<= 0 next-screen) (<= next-screen my-elscreen-tab-max))
	  (elscreen-goto next-screen))))

  ;; overwrite default keybinding <prefix> a -z to the
  ;; my-elscreen-jump. use <prefix> C-a .. C-z version as alternative.
  (when (> (- my-elscreen-tab-max 10) 0)
    (dotimes (i (- my-elscreen-tab-max 10))
      (define-key elscreen-map (kbd (format "%c" (+ ?a i))) 'my-elscreen-jump))
    (define-key elscreen-map (kbd "C-b") 'elscreen-find-and-goto-by-buffer))

  ;; *. ELSCREEN MINIBUFFER TAB
  ;; ------------------------------------------------------------
  ;;
  ;; Show tab to the minibuffer.
  ;;
  ;; originally, this features showed tab information of elscreen to the
  ;; mini-buffer by using timer. but, emacs-navbar.el is more convenient
  ;; for that feature. so, i leave the feature of displaying tab to the
  ;; minibuffer.
  (setq elscreen-display-tab nil)

  (defvar my-elscreen-tab-update-interval 0.5)

  (defvar my-elscreen-tab-width 12)

  (defun my-elscreen-tab-show (&optional force-show)
    (let ((contents (or (current-message) "")))
      (when (or force-show
		(and (not (active-minibuffer-window))
		     (string= "" contents)))
	(let ((sep (propertize " "
			       'face 'elscreen-tab-background-face
			       'display '(space :width 0.5))))
	  (message "%s" (mapconcat #'identity (my-elscreen-tab-list) sep))))))

  (defun my-elscreen-tab-width ()
    (cond
     ((numberp my-elscreen-tab-width)
      my-elscreen-tab-width)
     (t
      (elscreen-tab-width))))

  (defun my-elscreen-tab-number (num)
    (cond
     ((>= num 10)
      (format "%c" (+ ?a (- num 10))))
     (t
      (format "%s" num))))

  (defun my-elscreen-tab-list ()
    (let ((slist   (sort (elscreen-get-screen-list) '<))
	  (s-alist (elscreen-get-screen-to-name-alist))
	  (cur-num (elscreen-get-current-screen))
	  (space   (propertize " " 'display '(space :width 0.5))))
      (mapcar #'(lambda (num)
		  (let* ((index (my-elscreen-tab-number num))
			 (label (elscreen-status-label num))
			 (name  (assoc-default num s-alist))
			 (width (my-elscreen-tab-width))
			 (tname (elscreen-truncate-screen-name name width t))
			 (ename (elscreen-tab-escape-% tname)))
		    (propertize (format "%s%s%s%s" index label space ename)
				'face (if (eq cur-num num)
					  'elscreen-tab-current-screen-face
					'elscreen-tab-other-screen-face))))
	      slist)))

  (defun my-elscreen-tab-show-forcibly ()
    (interactive)
    (my-elscreen-tab-show t))

  ;; overwrite the time displaying feature of default by the feature of
  ;; displaying tab forcibly. The time displaying feature does not use
  ;; since time shows in the mode-line.
  (define-key elscreen-map (kbd "C-t") 'my-elscreen-tab-show-forcibly)

  ;; The key bind <prefix> A is hard to use for me. So replace that by
  ;; keybind <prefix> C-,.
  (define-key elscreen-map (kbd "C-,") 'elscreen-screen-nickname)

  ;; C-d is easy to use than C-k for me.
  (define-key elscreen-map (kbd "C-d") 'elscreen-kill)

  ;; C-z C-z is suspend frame. This is useful in emacs nw.
  (define-key elscreen-map (kbd "C-z") 'suspend-frame)

  ;; switch elscreen tab by one stroke.
  (global-set-key (kbd "C-,") 'elscreen-previous)
  (global-set-key (kbd "C-.") 'elscreen-next)

  ;; *. ELSCREEN EXTENDED SCREEN LIST
  ;; ------------------------------------------------------------
  ;;
  ;; make window list like the tmux's C-b C-w.
  (defface my-elscreen-list-highlight-line
    '((((class color) (background dark))
       (:foreground "turquoise1" t))
      (((class color) (background light))
       (:foreground "turquoise4" t))
      (t (:bold t)))
    ""
    :group 'elscren)

  (defconst my-elscreen-list-mode-name "ELSCREEN/LIST")

  (defvar my-elscreen-list-buffer " *elscreen list*")

  (defvar my-elscreen-list-mode nil)

  (defvar my-elscreen-list-mode-hook nil)

  (defvar my-elscreen-list-line-face 'my-elscreen-list-highlight-line)

  (defvar my-elscreen-list-window nil)

  (defvar my-elscreen-list-mode-map
    (let ((keymap (make-sparse-keymap)))
      (suppress-keymap keymap)
      ;; TODO: add function that do the 'change screen name'.
      (define-key keymap (kbd "q")   'my-elscreen-list-quit)
      (define-key keymap (kbd "C-m") 'my-elscreen-list-select)
      (define-key keymap (kbd "m")   'my-elscreen-list-select)
      (define-key keymap (kbd ";")   'my-elscreen-list-select)
      (define-key keymap (kbd "n")   'my-elscreen-list-next)
      (define-key keymap (kbd "p")   'my-elscreen-list-prev)
      (define-key keymap (kbd "C-n") 'my-elscreen-list-next)
      (define-key keymap (kbd "C-p") 'my-elscreen-list-prev)
      (define-key keymap (kbd "j")   'my-elscreen-list-next)
      (define-key keymap (kbd "k")   'my-elscreen-list-prev)
      keymap))

  (defun my-elscreen-list-mode ()
    (kill-all-local-variables)
    (setq mode-name  my-elscreen-list-mode-name
	  major-mode 'my-elscreen-list-mode
	  buffer-read-only t)
    (use-local-map my-elscreen-list-mode-map)
    (run-hooks 'my-elscreen-list-mode-hook))

  (defun my-elscreen-list-screens ()
    (interactive)
    (let ((buff (get-buffer-create my-elscreen-list-buffer)))
      (unless (equal (current-buffer) buff)
	(save-window-excursion
	  (with-current-buffer buff
	    (let ((buffer-read-only nil)
		  (screen-list (sort (elscreen-get-screen-to-name-alist)
				     #'(lambda (a b)
					 (> (car b) (car a)))))
		  (screen-now  (elscreen-get-current-screen))
		  (screen-prev (elscreen-get-previous-screen))
		  num name)
	      (goto-char (point-min))
	      (erase-buffer)
	      (dolist (screen screen-list)
		(setq num  (car screen)
		      name (cdr screen))
		(insert (propertize (format "%s %s [%02d] %s\n"
					    " "
					    (cond
					     ((eq num screen-now)
					      "C")
					     ((eq num screen-prev)
					      "P")
					     (t
					      " "))
					    num name)
				    'face my-elscreen-list-line-face
				    'num num))))
	    (my-elscreen-list-mode)
	    (goto-char (point-min))))
	(setq my-elscreen-list-window (current-window-configuration))
	(switch-to-buffer buff))))

  (defun my-elscreen-list-select ()
    (interactive)
    (let ((num (get-text-property (point) 'num)))
      (when num
	(my-elscreen-list-quit)
	(elscreen-goto num))))

  (defun my-elscreen-list-quit ()
    (interactive)
    (let ((buff (get-buffer my-elscreen-list-buffer)))
      (when (and buff my-elscreen-list-window)
	(set-window-configuration my-elscreen-list-window)
	(setq my-elscreen-list-window nil))))

  (defun my-elscreen-list-next ()
    (interactive)
    (forward-line 1))

  (defun my-elscreen-list-prev ()
    (interactive)
    (forward-line -1))

  (define-key elscreen-map (kbd "C-w") 'my-elscreen-list-screens)

  (elscreen-start))

(provide 'my-elscreen)

;;; my-elscreen.el ends here
