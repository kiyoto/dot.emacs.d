;;; my-autoinsert.el --- my configuration (autoinsert)

;; Copyright (C) 2014-2015  HAMANO kiyoto

;; Author: HAMANO kiyoto <khiker.mail@gmail.com>
;; Keywords: .emacs init

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

(eval-when-compile
  (require 'my)
  (require 'autoinsert))

(my-require 'autoinsert
  (setq auto-insert-directory (format "%setc/autoinsert/" user-emacs-directory)
	auto-insert-alist
	(nconc '(("\\.c$"   . "default.c")
		 ("\\.tex$" . "default.tex"))
	       auto-insert-alist))

  ;; override existing C header file entry to do following.
  ;; * use UUID as include guard name instead of file name.
  ;; * insert the comment of include guard name to the endif position.
  (add-to-list 'auto-insert-alist
	       '(("\\.\\([Hh]\\|hh\\|hpp\\)\\'" . "C / C++ header")
		 (upcase (format "INCLUSION_GUARD_UUID_%04x%04x_%04x_%04x_%04x_%06x%06x"
				 (random (expt 16 4))
				 (random (expt 16 4))
				 (random (expt 16 4))
				 (random (expt 16 4))
				 (random (expt 16 4))
				 (random (expt 16 6))
				 (random (expt 16 6))))
		 "#ifndef " str "\n"
		 "#define " str "\n\n"
		 _ "\n\n#endif /* " str " */"))

  (add-hook 'find-file-not-found-functions 'auto-insert))

(provide 'my-autoinsert)

;;; my-autoinsert.el ends here
