;;; my-migemo.el --- my configuration (migemo)

;; Copyright (C) 2014-2015  HAMANO kiyoto

;; Author: HAMANO kiyoto <khiker.mail@gmail.com>
;; Keywords: .emacs init

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

(eval-when-compile
  (require 'my)
  (require 'migemo)
  (declare-function migemo-init "migemo"))

(cond
 ((executable-find (my-place-assoc my-migemo-bin-path))
  (my-require 'migemo
    (setq-default migemo-dictionary (my-place-assoc my-migemo-dictionary-path))

    (setq migemo-command (my-place-assoc my-migemo-bin-path)
	  migemo-options '("-q" "--emacs")
	  migemo-user-dictionary nil
	  migemo-regex-dictionary nil
	  migemo-coding-system 'utf-8-unix
	  migemo-pattern-alist-file (format "%s.migemo-pattern"
					    user-emacs-directory)
	  migemo-frequent-pattern-alist-file (format "%s.migemo-frequent"
						     user-emacs-directory))

    ;; init migemo in startup.
    (migemo-init)

    ;; do not use cache. because of this makes emacs shutdown process
    ;; very very slow.
    (setq migemo-pattern-alist-file nil
	  migemo-pattern-alist-length 1024
	  migemo-accept-process-output-timeout-msec 80)))
 (t
  (message "W: cmigemo not found.")))

(provide 'my-migemo)

;;; my-migemo.el ends here
