;;; my-server.el --- my configuration (server)

;; Copyright (C) 2014-2015  HAMANO kiyoto

;; Author: HAMANO kiyoto <khiker.mail@gmail.com>
;; Keywords: .emacs init

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

(eval-when-compile
  (require 'my)
  (require 'server)
  (declare-function server-running-p "server"))

(my-require 'server
  ;; change server socket file location not to be removed that file by
  ;; tmpwatch.
  (defvar my-server-socket-dir (format "%stmp" user-emacs-directory))
  (unless (file-directory-p my-server-socket-dir)
    (make-directory my-server-socket-dir))
  (setq server-socket-dir
	(format "%s/emacs%d" my-server-socket-dir (user-uid)))
  (cond
   ((not (eq t (server-running-p server-name)))
    (server-start))
   (t
    (message ";; `server-start': start FAILED"))))

(provide 'my-server)

;;; my-server.el ends here
