;;; my-startup.el --- my configuration (startup)

;; Copyright (C) 2014-2015  HAMANO kiyoto

;; Author: HAMANO kiyoto <khiker.mail@gmail.com>
;; Keywords: .emacs init

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

(defvar my-scratch-save-directory (format "%sbak" user-emacs-directory)
  "directory that stores *scratch* files.")

(defun my-scratch-save-kill-emacs-hook ()
  "Save *scratch* buffer when `kill-emacs' was called."
  (let ((scratch-buf (get-buffer "*scratch*")))
    (when scratch-buf
      (with-current-buffer scratch-buf
	(when (not (string= initial-scratch-message (buffer-string)))
	  (write-file (format "%s/*scratch*%s"
			      (directory-file-name my-scratch-save-directory)
			      (format-time-string "%Y%m%d%H%M%S"))
		      nil))))))

(defun my-scratch-save-kill-buffer-hook ()
  "Save *scratch* buffer when *scratch* buffer was killed."
  (when (equal (current-buffer) (get-buffer "*scratch*"))
    (when (not (string= initial-scratch-message (buffer-string)))
      (write-file (format "%s/*scratch*%s"
			  (directory-file-name my-scratch-save-directory)
			  (format-time-string "%Y%m%d%H%M%S"))
		  nil))))

(setq inhibit-startup-screen t
      initial-scratch-message ";; -*- Coding:utf-8-unix; Mode:Emacs-lisp -*-\n
\n;; *scratch* ends here\n"
      initial-major-mode 'emacs-lisp-mode)

(add-hook 'kill-emacs-hook  'my-scratch-save-kill-emacs-hook)
(add-hook 'kill-buffer-hook 'my-scratch-save-kill-buffer-hook)

(provide 'my-startup)

;;; my-startup.el ends here
