;;; my-rust.el --- my configuration (rust)

;; Copyright (C) 2015  HAMANO kiyoto

;; Author: HAMANO kiyoto <khiker.mail@gmail.com>
;; Keywords: .emacs init rust

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; you should install following packages.
;;
;; * racer   ... https://github.com/phildawes/racer.git
;;
;; if you want to use racer, you should set racer-rust-src-path
;; variable to proper value.
;;
;; (setq racer-rust-src-path "/home/foo/src/rust/src")

;;; Code:

(eval-when-compile
  (require 'my)
  (require 'rust-mode)
  (require 'racer)
  (require 'cargo))

(my-require '(rust-mode cargo)
  (add-hook 'rust-mode-hook 'cargo-minor-mode)

  (defun my-rust-hook-func ()
    (setq indent-tabs-mode nil))
  (add-hook 'rust-mode-hook  #'my-rust-hook-func)

  (my-require 'racer
    (when (executable-find racer-cmd)
      (add-hook 'rust-mode-hook  #'racer-mode)
      (add-hook 'racer-mode-hook #'eldoc-mode)

      (declare-function my-racer-hook-func "my-rust")
      (defun my-racer-hook-func ()
        (ac-racer-setup))
      (add-hook 'racer-mode-hook #'my-racer-hook-func))))

(provide 'my-rust)

;;; my-rust.el ends here
