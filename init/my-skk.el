;;; my-skk.el --- my configuration (skk)

;; Copyright (C) 2014-2015  HAMANO kiyoto

;; Author: HAMANO kiyoto <khiker.mail@gmail.com>
;; Keywords: .emacs init

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

(eval-when-compile
  (require 'my)
  (require 'skk)
  (declare-function skk-restart "skk"))

(setq skk-user-directory (format "%sddskk" user-emacs-directory))

(my-require 'skk
  (global-set-key (kbd "C-x j") 'skk-mode))

(my-require 'skk-mazegaki
  (setq skk-rom-kana-rule-list
	(nconc skk-rom-kana-rule-list
	       '(("h," nil ("、" . "、"))
		 ("h." nil ("。" . "。"))
		 ("h/" nil ("／" . "／"))
		 ("h;" nil ("＼" . "＼"))
		 ("fj" nil skk-mazegaki-henkan)
		 ("z=" nil ("≠" . "≠"))
		 ("z(" nil "【")
		 ("z)" nil "】")
		 ("z{" nil "〔")
		 ("z}" nil "〕")
		 ("@"  nil "@"))))
  (skk-restart))

(provide 'my-skk)

;;; my-skk.el ends here
