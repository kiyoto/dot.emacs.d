;;; my-font-lock.el --- my configuration (font-lock)

;; Copyright (C) 2014-2015  HAMANO kiyoto

;; Author: HAMANO kiyoto <khiker.mail@gmail.com>
;; Keywords: .emacs init

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

(defface my-face-b-1
  '((t (:background "forest green")))
  "Face for zenkaku space."
  :group 'font-lock-faces)

(defface my-face-b-2
  '((t (:background "navy")))
  "Face for ."
  :group 'font-lock-faces)

(defface my-face-b-3
  '((t (:background "steel blue")))
  "Face for tab."
  :group 'font-lock-faces)

(defface my-face-b-4
  '((t (:background "gray8")))
  "Face for tab case 2."
  :group 'font-lock-faces)

(defvar my-face-b-1 'my-face-b-1
  "The variable specifies the face of zenkaku space.")

(defvar my-face-b-2 'my-face-b-2
  "The variable specifies the face of .")

(defvar my-face-b-3 'my-face-b-4
  "The variable specifies the face of tab char.")

(defvar my-font-lock-hilight-list '())

(defun my-font-lock-highlight-spaces (&optional args)
  "The advice for visibling the zenkaku space, , tab char and
trailing white spaces. "

  (unless (member major-mode my-font-lock-hilight-list)
    (font-lock-add-keywords
     major-mode
     '(("　" 0 my-face-b-1 append)
       ("" 0 my-face-b-2 append)
       ;; ;; do not highlight the tab as indent.
       ;; ("[^	\n\r]\\(	+\\)" 1 my-face-b-3 append)
       ("	" 0 my-face-b-3 append)))
    (add-to-list 'my-font-lock-hilight-list major-mode)))

(advice-add 'font-lock-mode :before 'my-font-lock-highlight-spaces)

(provide 'my-font-lock)

;;; my-font-lock.el ends here
