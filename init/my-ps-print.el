;;; my-ps-print.el --- my configuration (ps-print)

;; Copyright (C) 2015  HAMANO kiyoto

;; Author: HAMANO kiyoto <khiker.mail@gmail.com>
;; Keywords: .emacs init

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

(eval-when-compile
  (require 'my)
  (require 'ps-print))

(with-eval-after-load 'ps-print
  (setq ps-multibyte-buffer  'non-latin-printer
	ps-line-number       nil
	ps-paper-type        'a4
	ps-landscape-mode    t
	ps-number-of-columns 2
	ps-print-header      t
	ps-header-lines      1
	ps-left-header       (list #'(lambda ()
				       (let ((format "%Y/%m/%d %H:%M:%S"))
					 (concat  (format-time-string format)
						  " "
						  "<" (buffer-name) ">"))))
	ps-right-header      '("/pagenumberstring load")))

(provide 'my-ps-print)

;;; my-ps-print.el ends here
