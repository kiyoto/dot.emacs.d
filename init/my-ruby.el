;;; my-ruby.el --- my configuration (ruby)

;; Copyright (C) 2014-2015  HAMANO kiyoto

;; Author: HAMANO kiyoto <khiker.mail@gmail.com>
;; Keywords: .emacs init

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; robe requires following gems.
;;
;; * pry
;; * pry-doc
;; * method_source

;;; Code:

(eval-when-compile
  (require 'my)
  (require 'ruby-mode)
  (require 'robe)
  (require 'align)
  (declare-function robe-start "robe"))

;; align for Ruby
(my-require 'align
  (defvar my-align-ruby-rules-alist
    '((ruby-comma-delimiter
       (regexp . ",\\(\\s-*\\)[^# \t\n]")
       (repeat . t)
       (modes  . '(ruby-mode)))
      (ruby-hash-literal
       (regexp . "\\(\\s-*\\)=>\\s-*[^# \t\n]")
       (repeat . t)
       (modes  . '(ruby-mode)))
      (ruby-assignment-literal
       (regexp . "\\(\\s-*\\)=\\s-*[^# \t\n]")
       (repeat . t)
       (modes  . '(ruby-mode)))
      (ruby-xmpfilter-mark
       (regexp . "\\(\\s-*\\)# => [^#\t\n]")
       (repeat . nil)
       (modes  . '(ruby-mode))))
    "Align-mode rules for `ruby-mode'.")

  (dolist (i my-align-ruby-rules-alist)
    (add-to-list 'align-rules-list i)))

(defun my-ruby-mode-hook-func ()
  "Hook function for `ruby-mode-hook'."
  (subword-mode)

  (my-require 'ruby-end)

  ;; C-c C-s switch *ruby* buffer
  ;; M-.     jump to definition
  ;; M-,     jump back
  ;; C-c C-d see documentation
  ;; C-c C-k refresh Rails env
  ;; C-M-i   complete symbol at point
  (my-require '(inf-ruby robe)
    ;; force start inf-ruby and robe.
    (save-window-excursion
      (inf-ruby))
    (robe-mode)
    (robe-start)
    (ac-robe-setup))

  (my-require '(flycheck flycheck-tip)
    (define-key ruby-mode-map (kbd "C-c d") 'flycheck-tip-cycle))

  (define-key ruby-mode-map (kbd "C-m") 'electric-newline-and-maybe-indent))

(add-hook 'ruby-mode-hook 'my-ruby-mode-hook-func)

(provide 'my-ruby)

;;; my-ruby.el ends here
