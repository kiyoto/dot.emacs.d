;;; my-config-check-updates.el --- my configuration (check .emacs.d updates)

;; Copyright (C) 2016  HAMANO kiyoto

;; Author: HAMANO kiyoto <khiker.mail@gmail.com>
;; Keywords: .emacs init

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

(defvar my-config-check-updates nil)

(defvar my-config-check-updates-string nil)

(defcustom my-config-check-updates-interval (* 60 60)
  "Timer interval of checking the .emacs.d updates."
  :type  'integer
  :group 'my)

(defun my-config-check-updates-start ()
  "Start the checks of .emacs.d updates."
  (when my-config-check-updates
    (cancel-timer my-config-check-updates))
  (setq my-config-check-updates
	(run-with-idle-timer my-config-check-updates-interval
			     my-config-check-updates-interval
			     'my-config-check-updates))
  (add-to-list 'global-mode-string
	       'my-config-check-updates-string))

(defun my-config-check-updates-stop ()
  "Stop the checks of .emacs.d updates."
  (when my-config-check-updates
    (cancel-timer my-config-check-updates)
    (setq my-config-check-updates nil)
    (my-config-check-updates-mode-line nil)))

(defun my-config-check-updates-mode-line (str)
  (setq global-mode-string
      (delq 'my-config-check-updates-string global-mode-string))
  (when str
    (setq my-config-check-updates-string (format " %s" str))
    (add-to-list 'global-mode-string 'my-config-check-updates-string)))

(defun my-config-check-updates ()
  ;; TODO: to run bzr revno asynchronously.
  (let* ((default-directory user-emacs-directory)
	 (crevno (shell-command-to-string "bzr revno -q"))
	 (prevno (shell-command-to-string "bzr revno -q :parent")))
    (cond
     ((> (string-to-number prevno 10)
	 (string-to-number crevno 10))
      (my-config-check-updates-mode-line "[U]")
      (message ".emacs UPGRADE AVAILABLE (parent = %s, current = %s)"
	       crevno prevno))
     (t
      (my-config-check-updates-mode-line nil)))))

;; run timer for checking .emacs.d updates.
(my-config-check-updates-start)

;; check updates in the emacs startup sequence.
(my-config-check-updates)

(provide 'my-config-check-updates)

;;; my-config-check-updates.el ends here
