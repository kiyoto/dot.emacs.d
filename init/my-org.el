;;; my-org.el --- my configuration (org)

;; Copyright (C) 2015  HAMANO kiyoto

;; Author: HAMANO kiyoto <khiker.mail@gmail.com>
;; Keywords: .emacs init

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

(eval-when-compile
  (require 'my)
  (require 'org))

(my-require 'org
  (declare-function my-org-mode-hook "my-org")
  (defun my-org-mode-hook ()
    ;; clear bindings of C-,. i use that key as screen switching of
    ;; elscreen.
    (define-key org-mode-map (kbd "C-,") 'nil))

  (add-hook 'org-mode-hook #'my-org-mode-hook))


(provide 'my-org)

;;; my-org.el ends here
