;;; my-c-c++.el --- my configuration (C/C++)

;; Copyright (C) 2014-2015  HAMANO kiyoto

;; Author: HAMANO kiyoto <khiker.mail@gmail.com>
;; Keywords: .emacs init

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

;; OOo development coloring for C/C++
;;
;; - http://www.bookshelf.jp/pukiwiki/pukiwiki.php?
;;   C%2FC%2B%2B%A4%CE%A5%BD%A1%BC%A5%B9%A4%F2%C7%C9%BC%EA%A4%CB%BF%A7%A4%C5
;;   %A4%B1
;; - http://wiki.services.openoffice.org/wiki/Editor_Emacs

(eval-when-compile
  (require 'my)
  (require 'cpp)
  (require 'gud)
  (require 'gdb-mi)
  (require 'auto-complete)
  (require 'ac-c-headers)
  (require 'cc-mode)
  (require 'ggtags)
  (require 'flycheck)
  (require 'hideif)
  (declare-function c-indent-new-comment-line "cc-cmds")
  (declare-function c-indent-command "cc-cmds")
  (declare-function hif-find-ifdef-block "hideif")
  (declare-function show-ifdef-block "hideif")
  (declare-function hide-ifdef-block "hideif"))

(defface my-c-font-lock-dbg-face
  '((((class color) (background light))
     (:foreground "DeepPink"))
    (((class color) (background dark))
     (:foreground "Red3"))
    (t (:bold t :italic t)))
  "Font Lock mode face used for OOo Debug messages."
  :group 'font-lock-highlighting-faces)

(defface my-c-font-lock-brace-face
  '((((class color) (background light))
     (:foreground "Red2"))
    (((class color) (background dark))
     (:foreground "sienna1"))
    (t (:bold t :italic t)))
  "Font Lock mode face used for braces ()[]{} and the comma."
  :group 'font-lock-highlighting-faces)

(defface my-c-font-lock-bool-face
  '((((class color) (background light))
     (:foreground "forest green"))
    (((class color) (background dark))
     (:foreground "lime green"))
    (t (:bold t :italic t)))
  "Font Lock mode face used for boolean operators."
  :group 'font-lock-highlighting-faces)


(defvar my-c-font-lock-dbg-face 'my-c-font-lock-dbg-face
  "Face name to use for OOo Debug messages.")

(defvar my-c-font-lock-brace-face 'my-c-font-lock-brace-face
  "Face name to use for braces.")

(defvar my-c-font-lock-bool-face 'my-c-font-lock-bool-face
  "Face name to use for boolean operators.")


(defconst my-c-bm-additional-constant-keywords
  (cons
   (regexp-opt
    ;; coloring these words. oftenly use #define words
    (list "TRUE" "FALSE" "ON" "OFF") 'words)
   font-lock-constant-face))

(defconst my-c-bm-assert-keywords
  (cons
   (concat "\\<\\("
	   ;; coloring a debug relation process.
	   (regexp-opt
	    (list "PRINTF" "printf" "ASSERT"  "DBPRINTF" ))
	   "\\|DBG_\\sw+\\)\\>")
   my-c-font-lock-dbg-face))

(defconst my-c-bm-brace-keywords
  ;; coloring a parenthesis.
  (cons "[][(){}]" my-c-font-lock-brace-face))

(defconst my-c-bm-operator-keywords
  ;; coloring the symbols.
  (cons "[|&!<>+-.*/^~%=:?]+" my-c-font-lock-bool-face))


(defvar my-c-printf-debug-line
  (concat "printf(\"%s %s:%d\\n\", __func__, __FILE__, __LINE__);"
	  "fflush(stdout); /* debug */")
  "Inserting contents by printf debug.")

(defun my-c-insert-printf-debug (arg)
  "Insert printf debug line."
  (interactive "P")
  (cond
   (arg
    (call-interactively 'my-c-delete-debug-printf))
   (t
    (insert my-c-printf-debug-line)
    (indent-according-to-mode))))

(defun my-c-delete-reg-buffers (reg)
  "delete matched line by regular expression `reg'."
  (interactive)
  (let ((target-buffers (buffer-list))
	(delete-reg
	 '(lambda ()
	    (save-excursion
	      (progn
		(goto-char (point-min))
		(while (re-search-forward reg nil t)
		  (delete-region (save-excursion
				   (match-beginning 0)
				   (forward-line 0)
				   (point))
				 (match-end 0))))))))
    (save-current-buffer
      (mapcar (lambda (b)
		(set-buffer b)
		(funcall delete-reg)) target-buffers))))

(defun my-c-delete-debug-printf ()
  "Delete printf debug line."
  (interactive)
  (my-c-delete-reg-buffers (regexp-quote my-c-printf-debug-line))
  (indent-according-to-mode))

(defun my-c-insert-braces (arg)
  "do indent after the inserting a {}."
  (interactive "P")
  (insert-pair arg ?{ ?})
  (call-interactively 'c-indent-command))

(defun my-c-newline-and-indent ()
  "intelligence `newline-and-idnent'.
If you insert linefeed when point was on the '{', insert empty line
between the '{' and '}'. The point is on the empty line and empty line,
{ line and } line are indented.

If buffer was read-only and point was on ifdef line, execute
`my-hide-ifdef-block-toggle'."
  (interactive)
  (cond
   (buffer-read-only
    (unwind-protect
	(progn
	  (setq buffer-read-only nil)
	  (when (fboundp 'my-hide-ifdef-block-toggle)
	    (my-hide-ifdef-block-toggle)))
      (setq buffer-read-only t)))
   (t
    (cond
     ((and (not (bobp))
	   (= (char-before (point)) ?{))
      (save-excursion
	(delete-horizontal-space t)
	(c-indent-new-comment-line)
	(c-indent-new-comment-line))
      (forward-line)
      (c-indent-command))
     (t
      (c-indent-new-comment-line))))))

(defun my-c++-comment-dwim (arg)
  "insert /* */ style comment."
  (interactive "*P")
  (let ((comment-start "/* ")
	(comment-end   " */"))
    (comment-dwim arg)))

(defun my-c-enclose-if-0 (beg end)
  (interactive "r")
  (cond
   (mark-active
    (let* ((bol-point    #'(lambda (p text)
			     (save-excursion
			       (goto-char p)
			       (forward-line 0)
			       (prog1 (point)
				 (insert text)))))
	   (open-string  "#if 0")
	   (close-string "#endif /* 0 */")
	   start-point)
      (save-excursion
	(setq start-point
	      (funcall bol-point
		       (region-beginning) (format "%s\n" open-string)))
	(funcall bol-point
		 (region-end) (format "%s\n" close-string)))
      (goto-char (+ start-point (length open-string)))))
   (t
    (ignore))))

(font-lock-add-keywords
 'c++-mode
 (list my-c-bm-brace-keywords
       my-c-bm-operator-keywords
       my-c-bm-additional-constant-keywords
       my-c-bm-assert-keywords))

(font-lock-add-keywords
 'c-mode
 (list my-c-bm-brace-keywords
       my-c-bm-operator-keywords
       my-c-bm-additional-constant-keywords
       my-c-bm-assert-keywords))

;; highlight enclosure of the #if ... #endif.
(my-require 'cpp
  (setq cpp-known-face       'default
	cpp-unknown-face     nil
	cpp-face-type        'light
	cpp-known-writable   't
	cpp-unknown-writable 't

	;; highlight #if 0 ... #endif
	cpp-edit-list
	'(("0"     (background-color . "dim gray") nil both nil)
	  ("1" nil (background-color . "dim gray") nil both)
	  (""  nil nil both nil)))

  (defvar my-cpp-highlight-enable-modes '(c-mode c++-mode)
    "List of major-mode that enables executing `cpp-higlight-buffer'
after executing `save-buffer'.")

  (defun my-cpp-highlight-buffer ()
    (when (member major-mode my-cpp-highlight-enable-modes)
      (cpp-highlight-buffer t)))

  (add-hook 'after-save-hook 'my-cpp-highlight-buffer))

(my-require 'hideif
  (defun my-hide-ifdef-block-toggle ()
    "Toggle hiding status of current line."
    (interactive)
    (let ((ifdef (condition-case err
		     (hif-find-ifdef-block)
		   (error nil nil)))
	  hiding)
      (when (and ifdef (consp ifdef))
	(dolist (o (overlays-in (car ifdef) (cdr ifdef)))
	  (when (overlay-get o 'hide-ifdef)
	    (setq hiding t)))
	(cond
	 (hiding
	  (show-ifdef-block))
	 (t
	  (hide-ifdef-block)))))))

(my-require '(gud gdb-mi)
  ;; start with two windows.
  (setq gdb-many-windows t)

  (defun my-gdb-mode-hook-func ()
    "Hook function for `gdb-mode-hook'."
    (gud-tooltip-mode t))

  (add-hook 'gdb-mode-hook 'my-gdb-mode-hook-func))

(my-require 'cc-mode
  (defun my-co-mode-common-hook-func ()
    (when (derived-mode-p 'c-mode 'c++-mode 'java-mode)
      (when (executable-find "gtags")
	;; M-. M-, M-* M-n M-p
	(my-require 'ggtags
	  (setq ggtags-split-window-function #'split-window-vertically)
	  (ggtags-mode 1)))))

  (defun my-c-mode-hook-func ()
    "Hook function for `c-mode-hook'."
    (my-require '(find-file ac-c-headers)
      (setq cc-search-directories '("." "/usr/include"))

      ;; XXX: In version 1.0.0, the function that uses following
      ;; prototype declaration (function name with pointer asterisk)
      ;; does not appear in the completion candidate.
      ;;
      ;;   char *strcpy(...)
      ;;
      ;; Because, auto-complete-c-header uses regular expression
      ;; \\_<[a-zA-Z]*\\_>.
      ;; (add-to-list 'ac-sources 'ac-source-c-header-symbols t)
      (add-to-list 'ac-sources 'ac-source-c-headers))

    (add-to-list 'ac-sources 'ac-source-semantic)

    (my-require 'jump
      (define-key c-mode-map (kbd "C-c j") 'jump-symbol-at-point)
      (define-key c-mode-map (kbd "C-c b") 'jump-back))

    ;; XXX: whether to use the auto-complete-clang-async.el.
    ;; XXX: whether to use the c-eldoc.el.

    (my-require '(flycheck flycheck-tip)
      (setq flycheck-gcc-language-standard   "gnu99"
	    flycheck-clang-language-standard "gnu99")
      (define-key c-mode-map (kbd "C-c d") 'flycheck-tip-cycle))

    (c-set-style "linux")

    (cpp-highlight-buffer t)

    (define-key c-mode-map (kbd "C-c D") 'my-c-insert-printf-debug)
    (define-key c-mode-map (kbd "C-c H") 'ff-find-other-file)

    (define-key c-mode-map (kbd "C-m")   'my-c-newline-and-indent)
    (define-key c-mode-map (kbd "C-c #") 'my-c-enclose-if-0)
    (define-key c-mode-map (kbd "{")   'my-c-insert-braces))

  (defun my-c++-mode-hook-func ()
    "Hook function for `c++-mode-hook'."
    (my-require 'flycheck
      (setq flycheck-gcc-language-standard   "gnu++11"
	    flycheck-clang-language-standard "gnu++11")
      (define-key c++-mode-map (kbd "C-c d") 'flycheck-tip-cycle))

    (define-key c++-mode-map (kbd "C-c D") 'my-insert-printf-debug)
    (define-key c++-mode-map (kbd "C-c H") 'ff-find-other-file)
    (define-key c++-mode-map (kbd "C-M-;") 'my-c++-comment-dwim))

  (add-hook 'c-mode-common-hook 'my-co-mode-common-hook-func)
  (add-hook 'c-mode-hook 'my-c-mode-hook-func)
  (add-hook 'c++-mode-hook 'my-c++-mode-hook-func))

(provide 'my-c-c++)

;;; my-c-c++.el ends here
