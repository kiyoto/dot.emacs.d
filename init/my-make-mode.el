;;; my-make-mode.el --- my configuration (make-mode) -*- lexical-binding: t; -*-

;; Copyright (C) 2015  HAMANO Kiyoto

;; Author: HAMANO Kiyoto <khiker.mail@gmail.com>
;; Keywords: .emacs init makefile

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

(eval-when-compile
  (require 'my)
  (require 'make-mode)
  (declare-function makefile-browser-fill "make-mode")
  (declare-function makefile-browser-start-interaction "make-mode"))

(my-require 'make-mode
  ;; actually, i want to do highlight like the show-paren-mode.
  ;; but, it is ado to implemet.
  (defun my-makefile-mark-if-block ()
    "mark current ifeq|ifneq ... endif block."
    (interactive)
    (let (stack)
      (save-excursion
	(save-restriction
	  (goto-char (point-min))
	  (let (lis)
	    (while (re-search-forward (format "^\\(%s\\)"
					      (mapconcat #'identity
							 '("ifeq"  "ifneq"
							   "ifdef" "ifndef"
							   "else"  "endif")
							 "\\|"))
				      nil t)
	      (let ((str  (match-string    0))
		    (spos (match-beginning 0))
		    (epos (match-end       0))
		    elem)
		(cond
		 ((string= str "endif")
		  (when (setq elem (pop lis))
		    (push (list (nth 1 elem) epos) stack)))
		 ((string= str "else")
		  (when (setq elem (pop lis))
		    (push (list (nth 1 elem) epos) stack))
		  (push (list str spos epos) lis))
		 (t
		  (push (list str spos epos) lis))))))))
      (when (catch 'fin
	      (dolist (scope (nreverse stack))
		(when (and (< (nth 0 scope) (point))
			   (> (nth 1 scope) (point)))
		  (push-mark (point))
		  (push-mark (nth 1 scope) nil t)
		  (goto-char (nth 0 scope))
		  (throw 'fin scope)))))))

  (defun my-makefile-browse (targets macros)
    (if (zerop (+ (length targets) (length macros)))
	(progn
	  (beep)
	  (message "No macros or targets to browse! Consider running 'makefile-pickup-everything\'"))
      (let ((browser-buffer (get-buffer-create makefile-browser-buffer-name)))
        (pop-to-buffer browser-buffer)
	(makefile-browser-fill targets macros)
	;; shrink-window-if-larger-than-buffer function is uncogenial to
	;; emacs-navibar's window (navbar window is enlarged). so,
	;; commented out the call of that function by nadvice.
	;; (shrink-window-if-larger-than-buffer)
	(setq-local makefile-browser-selection-vector
		    (make-vector (+ (length targets) (length macros)) nil))
	(makefile-browser-start-interaction))))
  (advice-add 'makefile-browse :override 'my-makefile-browse)

  (defun my-makefile-mode-hook-func ()
    "Hook function for `makefile-mode-hook'"
    (define-key makefile-mode-map (kbd "C-c b") 'my-makefile-mark-if-block))

  (add-hook 'makefile-mode-hook 'my-makefile-mode-hook-func))

(provide 'my-make-mode)

;;; my-make-mode.el ends here
