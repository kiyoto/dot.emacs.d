;;; my-latex.el --- my configuration (latex)

;; Copyright (C) 2014-2015  HAMANO kiyoto

;; Author: HAMANO kiyoto <khiker.mail@gmail.com>
;; Keywords: .emacs init

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

(eval-when-compile
  (require 'my)
  (require 'tex)
  (require 'tex-jp)
  (require 'preview))

(with-eval-after-load 'tex
  (add-to-list 'auto-mode-alist (cons "\\.tex$" 'TeX-mode))

  ;; For japanese TeX
  (setq TeX-default-mode 'japanese-latex-mode)

  ;; default class is jsarticle
  (setq japanese-TeX-command-default   "pTeX"
	japanese-LaTeX-command-default "pLaTeX"
	japanese-LaTeX-default-style   "jsarticle"
	preview-image-type 'dvipng)

  (defvar my-TeX-command-alist
    '(("acroread" "acroread '%s.pdf' "  TeX-run-command t nil)
      ("PDF(dvipdfmx)" "dvipdfmx '%s' " TeX-run-command t nil)
      ("PDF(dvipdf)" "dvipdf '%s' "     TeX-run-command t nil)
      ("evince" "evince '%s.pdf' "      TeX-run-command t nil)
      ("xpdf" "xpdf '%s.pdf' "          TeX-run-command t nil)))

  (dolist (i my-TeX-command-alist)
    (add-to-list 'TeX-command-list i))

  (defun my-TeX-mode-hook-func ()
    "Hook function for `TeX-mode-hook'."
    nil)

  (add-hook 'TeX-mode-hook 'my-TeX-mode-hook-func))

(provide 'my-latex)

;;; my-latex.el ends here
