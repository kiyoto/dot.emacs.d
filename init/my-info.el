;;; my-info.el --- my configuration (info)

;; Copyright (C) 2014-2015  HAMANO kiyoto

;; Author: HAMANO kiyoto <khiker.mail@gmail.com>
;; Keywords: .emacs init

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

(eval-when-compile
  (require 'my)
  (require 'info)
  (require 'imenu))

(my-require 'info
  (defvar my-info-directory-list
    `("~/local/share/info"
      ,(format "%sinfo" user-emacs-directory)))

  (dolist (i my-info-directory-list)
    (when (file-exists-p i)
      (add-to-list 'Info-additional-directory-list
		   (expand-file-name i))))

  ;; imenu for info
  (defvar my-info-imenu-generic-expression
    '((nil "^\\* \\(\\(\\w* ?\\)*\\):" 1)))

  (defun my-Info-mode-hook-func ()
    "Hook function for Info-mode."
    (setq imenu-generic-expression my-info-imenu-generic-expression)
    (setq imenu-auto-rescan t)
    (define-key Info-mode-map (kbd "a")   'Info-follow-nearest-node)
    (define-key Info-mode-map (kbd "i")   'recenter)
    (define-key Info-mode-map (kbd "I")   'icicle-Info-index-cmd)
    (define-key Info-mode-map (kbd "h")   'backward-char)
    (define-key Info-mode-map (kbd "j")   'next-line)
    (define-key Info-mode-map (kbd "k")   'previous-line)
    (define-key Info-mode-map (kbd "l")   'forward-char)
    (define-key Info-mode-map (kbd "u")   'Info-scroll-down)
    (define-key Info-mode-map (kbd "U")   'Info-up)
    (define-key Info-mode-map (kbd ";")   'Info-follow-nearest-node)
    (define-key Info-mode-map (kbd "/")   'Info-history-back)
    (define-key Info-mode-map (kbd "\\")  'Info-history-forward)
    (define-key Info-mode-map (kbd "C-h") 'Info-help))

  (add-hook 'Info-mode-hook 'my-Info-mode-hook-func))

(provide 'my-info)

;;; my-info.el ends here
