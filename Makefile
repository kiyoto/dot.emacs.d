#
# Install dot.emacs.d
#

EMACS		= emacs
EMACA_BATCH_CODE= "(progn (require 'my) \
	(package-initialize) (my-load-path-set-recursively \"./site-lisp\"))"
EMACS_BATCH_OPT	= --batch -L . -L ./init -eval $(EMACA_BATCH_CODE)

FIND	= find
UNAME	:= $(shell uname -s)
ifeq ($(UNAME),Darwin)
	FIND	= gfind
endif

USER_EL	=	init.el init-requires.el envdep.el custom.el \
		$(shell $(FIND) -maxdepth 1 -name mew-rc.el) \
		${wildcard init/my*.el}
EL	=	${wildcard site-lisp/*/*.el}
ELC	=	$(USER_EL:.el=.elc) $(EL:.el=.elc)

RM	= rm -f
LN	= ln -sf
TOUCH	= touch

.PHONY: all list compile clean

all:
	@echo "make list    : list all target emacs lisp files"
	@echo "make install : install this to ~/ (make symlink) and compile."
	@echo "make compile : compile *.el files."
	@echo "make clean   : remove *.elc files."
	@echo "make wc      : count line number of all user defined el file."

list:
	@ls $(EL) $(USER_EL) | xargs -I {} echo {}

preinstall:
	$(TOUCH) envdep.el
	@if [ -L "$(HOME)/.emacs.d" ]; then \
		$(RM) $(HOME)/.emacs.d; \
	fi
	$(LN) $(CURDIR) $(HOME)/.emacs.d

install: preinstall archive compile

archive:
	$(EMACS) $(EMACS_BATCH_OPT) -l custom.el -l init/my-package.el

compile: $(ELC)
	@find elpa/mew* -name "*.elc" | xargs rm -f

clean:
	$(RM) $(ELC)

wc:
	@cat $(USER_EL) ddskk/init | \
		egrep -v -e '^;' | egrep -v -e '^[ \t]*$$' | wc -l

%.elc: %.el
	$(EMACS) $(EMACS_BATCH_OPT) -f batch-byte-compile $<

